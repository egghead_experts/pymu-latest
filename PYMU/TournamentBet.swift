//
//  TournamentBet.swift
//  PYMU
//
//  Created by Egghead IOS3 on 08/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit
import coinbase_official

class TournamentBet: UIViewController, UIGestureRecognizerDelegate , UIAlertViewDelegate, UIScrollViewDelegate, UICollectionViewDataSource , UICollectionViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var Btn_Back: UIButton!
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var GamePlace: UILabel!
    
    @IBOutlet var SelectionView: UIView!
    @IBOutlet var collection: UICollectionView!
    
    @IBOutlet var BetView: UIView!
    @IBOutlet var WON_Btn: UIButton!
    @IBOutlet var LOSE_Btn: UIButton!
    @IBOutlet var LoseBet: UILabel!
    @IBOutlet var Btn_LB_UP: UIButton!
    @IBOutlet var Btn_LB_DOWN: UIButton!
    
    @IBOutlet var Btn_PYMU: UIButton!
    
    @IBOutlet var PopupView: UIView!
    @IBOutlet var AlertView: UIView!
    @IBOutlet var AlertMessage: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    var timer = NSTimer()
    var LB_UP_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var LB_DOWN_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    
    var LiveData:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
    
    var Tournament_Id:Int = 0
    var User_Id:Int = 0
    var User_Name:String = ""
    var Artist_Id:Int = -1
    var ArtistIndex:Int = -1
    var Bet_For:String = ""
    var Point:Int = 0
    var Amount:Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setViewData()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TournamentBet.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
    }
    
    
    func setViewData()
    {
        GamePlace.text=LiveData["tournament_name"]! as? String
        self.TitleLbl.text="Bet"

        let layout:UICollectionViewFlowLayout=UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing=0.0
        layout.minimumLineSpacing=0.0
        layout.scrollDirection=UICollectionViewScrollDirection.Horizontal
        collection.collectionViewLayout=layout
        
        PopupView.hidden=true
        contentView.bringSubviewToFront(PopupView)
        
        LoseBet.text="0"
        self.WIN_Btn_Clicked(WON_Btn)
        
        LB_UP_longPress.addTarget(self, action: #selector(TournamentBet.UP_longPressed(_:)))
        Btn_LB_UP.addGestureRecognizer(LB_UP_longPress)
        
        LB_DOWN_longPress.addTarget(self, action: #selector(TournamentBet.DOWN_longPressed(_:)))
        Btn_LB_DOWN.addGestureRecognizer(LB_DOWN_longPress)
    }
    
    @IBAction func BackBtn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (LiveData["artists"]?.count)!
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell: UICollectionViewCell=collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath)
        
        var art:Array<String> = LiveData["artists"]!.objectAtIndex(indexPath.row) as! Array<String>

        let teamImg:UIImageView = cell.viewWithTag(11) as! UIImageView
        teamImg.setImageWithUrl(NSURL(string:art[2])!, placeHolderImage: UIImage(named:"team_bg"))
        teamImg.layer.cornerRadius=30
        teamImg.layer.masksToBounds = true
        
        let teamFrm:UIImageView = cell.viewWithTag(22) as! UIImageView
        if ArtistIndex == indexPath.row
        {
            teamFrm.image=UIImage(named: "team_frm_in_sel.png")
        }
        else
        {
            teamFrm.image=UIImage(named: "team_frm_in.png")
        }
        
        let teamName:UILabel = cell.viewWithTag(33) as! UILabel
        teamName.text=art[1]
        
        cell.contentView.frame = cell.bounds
        cell.backgroundColor=UIColor.clearColor()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var art:Array<String> = LiveData["artists"]!.objectAtIndex(indexPath.row) as! Array<String>

        ArtistIndex=indexPath.row
        Artist_Id=Int(art[0])!
        collection.reloadData()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width: self.SelectionView.frame.width/2.5 , height: self.SelectionView.frame.height/2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    // --------------------
    //   *** Bet View ***
    // --------------------
    
    @IBAction func WIN_Btn_Clicked(sender: AnyObject)
    {
        WON_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        LOSE_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        WON_Btn.userInteractionEnabled=false
        LOSE_Btn.userInteractionEnabled=true
        WON_Btn.tag=1
        LOSE_Btn.tag=0
    }
    @IBAction func LOSE_Btn_Clicked(sender: AnyObject)
    {
        WON_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        LOSE_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        WON_Btn.userInteractionEnabled=true
        LOSE_Btn.userInteractionEnabled=false
        WON_Btn.tag=0
        LOSE_Btn.tag=1
    }
    
    @IBAction func Btn_UP_Clicked(sender: AnyObject)
    {
        LoseBet.text="\(Int(LoseBet.text!)! + 1)"
    }
    
    func UP_longPressed(sender: UILongPressGestureRecognizer)
    {
        if ( sender.state == UIGestureRecognizerState.Began )
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(TournamentBet.UpAction), userInfo: nil, repeats: true)
        }
        if ( sender.state == UIGestureRecognizerState.Ended )
        {
            timer.invalidate()
        }
    }
    
    func UpAction()
    {
        LoseBet.text="\(Int(LoseBet.text!)! + 1)"
    }
    
    @IBAction func Btn_DOWN_Clicked(sender: AnyObject)
    {
        if Int(LoseBet.text!) > 0
        {
            LoseBet.text="\(Int(LoseBet.text!)! - 1)"
        }
    }
    
    func DOWN_longPressed(sender: UILongPressGestureRecognizer)
    {
        if ( sender.state == UIGestureRecognizerState.Began )
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(TournamentBet.DownAction), userInfo: nil, repeats: true)
        }
        if ( sender.state == UIGestureRecognizerState.Ended )
        {
            timer.invalidate()
        }
    }
    
    func DownAction()
    {
        if Int(LoseBet.text!) > 0
        {
            LoseBet.text="\(Int(LoseBet.text!)! - 1)"
        }
    }
    
    @IBAction func PYMU_Btn_Clicked(sender: AnyObject)
    {
        Bet_For = ""
        Point = 0
        Amount = 0
        
        var msg:String = ""
        
        if Artist_Id == -1
        {
            msg = "Please Select any Team/Player"
        }
        else if Int(LoseBet.text!) <= 0
        {
            msg = "Enter valid Bet amount"
        }
        else
        {
            if WON_Btn.tag == 1
            {
                Bet_For="win"
            }
            else if LOSE_Btn.tag == 1
            {
                Bet_For="lose"
            }
            
            Point=0
            Amount=Int(LoseBet.text!)!
        }
        
        if msg == ""
        {
            UIView.animateWithDuration(0.2, animations:
                {
                    self.PopupView.hidden=false
                    self.AlertView.layer.masksToBounds=true
                    self.AlertMessage.text="Are you sure want to bet with \(self.User_Name)?"
                    self.PopupView.alpha=0.7
                    self.PopupView.alpha=1.0
                },
                completion:
                {(value: Bool) in
            })
        }
        else
        {
            let alertController = UIAlertController(title: "PYMU", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func Alert_Bet_Clicked(sender: AnyObject)
    {
        CoinbaseOAuth.startOAuthAuthenticationWithClientId(CLIENT_ID , scope: "user balance", redirectUri: "com.egghead.pymu.coinbase-oauth://coinbase-oauth", meta: nil)
        NSUserDefaults.standardUserDefaults().setObject("TournamentBet" , forKey: "coinbase")

    }
    
    func authenticationComplete(response:[String : AnyObject])
    {
        Tournament_Id=LiveData["id"] as! Int
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "tournament_id=\(Tournament_Id)&artist_id=\(Artist_Id)&user_id=\(User_Id)&bet_for=\(Bet_For)&points=\(Point)&bet_amount=\(Amount)"
        
        var dataDict=ApiObj.CallPostApi("user/bet/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        
                        if (NSUserDefaults.standardUserDefaults().objectForKey("BettingView") as! String) == "Home"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("BettingView") as! String) == "SelectMatch"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                        }
                    },
                                           completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    @IBAction func Alert_Cancel_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopupView.hidden=true
            },
            completion:
            {(value: Bool) in
        })
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
