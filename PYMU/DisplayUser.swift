//
//  DisplayUser.swift
//  PYMU
//
//  Created by Egghead IOS3 on 10/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit

class DisplayUser: UIViewController, UIGestureRecognizerDelegate , UIAlertViewDelegate, UIScrollViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var Btn_Back: UIButton!
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var GamePlace: UILabel!
    
    @IBOutlet var Team1_Img: UIImageView!
    @IBOutlet var Team1_Frm: UIImageView!
    @IBOutlet var Team2_Img: UIImageView!
    @IBOutlet var Team2_Frm: UIImageView!
    @IBOutlet var Team1_Name: UILabel!
    @IBOutlet var Team2_Name: UILabel!
    @IBOutlet var Vs_Img: UIImageView!
    
    @IBOutlet var TournamentImg: UIImageView!
    @IBOutlet var TournamentFrm: UIImageView!
    
    @IBOutlet var UserView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var UserTable: UITableView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    var LiveData:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
    var AllUser:Array<AnyObject> = []
    var AllUserName:Array<AnyObject> = []
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setViewData()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DisplayUser.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = UIScreen.mainScreen().bounds.size.height
        ContentView_Height.constant = UIScreen.mainScreen().bounds.size.height
    }
    
    func hiddenHader(flag:Bool)
    {
        Team1_Frm.hidden=flag
        Team1_Img.hidden=flag
        Team1_Name.hidden=flag
        
        Vs_Img.hidden=flag
        
        Team2_Frm.hidden=flag
        Team2_Img.hidden=flag
        Team2_Name.hidden=flag
        
        TournamentFrm.hidden = !flag
        TournamentImg.hidden = !flag
    }
    
    func setViewData()
    {
        GamePlace.text=LiveData["tournament_name"]! as? String
        
        if LiveData["tournament_type"] as! String == "Tournament"
        {
            self.hiddenHader(true)
            
            TournamentImg.setImageWithUrl(NSURL(string:LiveData["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
            TournamentImg.layer.cornerRadius=27
            TournamentImg.layer.masksToBounds = true
        }
        else if LiveData["tournament_type"] as! String == "Match"
        {
            self.hiddenHader(false)
            
            var art1:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
            var art2:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
            
            Team1_Name.text=art1[1]
            Team2_Name.text=art2[1]
            
            Team1_Img.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
            Team2_Img.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
            
            Team1_Img.layer.cornerRadius=27
            Team1_Img.layer.masksToBounds = true
            Team2_Img.layer.cornerRadius=27
            Team2_Img.layer.masksToBounds = true
        }
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = LiveData["id"] as! String

        var dataDict=ApiObj.CallPostApi("user/openbetusers/\(urlStr)", postStr: "tournament_id=\(postString)")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                AllUser=[]
                AllUserName=[]
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var user:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    
                    user["user_name"] = arr["user_name"]!.string!
                    user["user_image"] = arr["user_image"]!.string!
                    user["bet_id"] = arr["bet_id"]!.string!
                    user["artist_id"] = arr["artist_id"]!.string!
                    user["bet_amount"] = arr["bet_amount"]!.string!
                    user["bet_for"] = arr["bet_for"]!.string!
                    user["points"] = arr["points"]!.string!

                    AllUser.insert(user, atIndex: AllUser.count)
                    AllUserName.insert(user, atIndex: AllUserName.count)
                }
                UserTable.reloadData()
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        let predicate=NSPredicate(format: "SELF.user_name CONTAINS[cd] %@", searchText)
        let arr=(AllUserName as NSArray).filteredArrayUsingPredicate(predicate)
        
        if arr.count > 0
        {
            AllUser.removeAll(keepCapacity: true)
            AllUser=arr
        }
        else
        {
            AllUser=AllUserName
        }
        UserTable.reloadData()
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar){}
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar){}
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        self.searchBar.resignFirstResponder()
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return AllUser.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 48
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let identifier = "UserCell"
        let cell: UserCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? UserCell
        
        let headerView=UIView()
        headerView.backgroundColor=UIColor.clearColor()
        headerView.backgroundColor=UIColor(patternImage: UIImage(named:"user_ra_bg_i6.png")!)
        cell.backgroundView=headerView
        cell.backgroundColor=UIColor.clearColor()
        
        let Data:Dictionary<String,AnyObject> = AllUser[indexPath.section] as! Dictionary<String, AnyObject>
        
        cell.imgUser.setImageWithUrl(NSURL(string:Data["user_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
        cell.imgUser.layer.cornerRadius=19
        
        cell.lblName.text=Data["user_name"] as? String
        
        cell.Status.hidden=true
        
        cell.lblDoler.hidden=false
        cell.lblDoler.text="$\(Data["bet_amount"] as! String)"

        let selectView=UIView()
        selectView.backgroundColor=UIColor.blackColor()
        cell.selectedBackgroundView=selectView
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        searchBar.resignFirstResponder()

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let Data:Dictionary<String,AnyObject> = AllUser[indexPath.section] as! Dictionary<String, AnyObject>
        self.LiveData["user_name"] = Data["user_name"] as! String
        self.LiveData["bet_id"] = Data["bet_id"] as! String
        self.LiveData["artist_id"] = Data["artist_id"] as! String
        self.LiveData["bet_amount"] = Data["bet_amount"] as! String
        self.LiveData["bet_for"] = Data["bet_for"] as! String
        self.LiveData["points"] = Data["points"] as! String
        
        if LiveData["tournament_type"] as! String == "Tournament"
        {
            self.performSegueWithIdentifier("DisplayTournamentClicked", sender: self)
        }
        else if LiveData["tournament_type"] as! String == "Match"
        {
            self.performSegueWithIdentifier("DisplayMatchClicked", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "DisplayMatchClicked"
        {
            let secondViewController = (segue.destinationViewController as! DisplayMatch)
            secondViewController.LiveData=self.LiveData
        }
        else if segue.identifier == "DisplayTournamentClicked"
        {
            let secondViewController = (segue.destinationViewController as! DisplayTournament)
            secondViewController.LiveData=self.LiveData
        }
    }
    
    @IBAction func BackBtn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        searchBar.resignFirstResponder()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
