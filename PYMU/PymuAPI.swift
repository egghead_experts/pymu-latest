//
//  PymuAPI.swift
//  PYMU
//
//  Created by Apple on 29/07/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

extension NSMutableData
{
    func appendString(string: String)
    {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}
//http://pymu.winbrainindia.com/rest
//http://www.genexity.com/rest/
class PymuAPI: NSObject , NSXMLParserDelegate
{
    func CallGetApi(str: String  ) -> JSON
    {
        let url = NSURL(string: "http://pymu.winbrainindia.com/rest/"+str)
        let request = NSURLRequest(URL: url!)
        let data = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
        
        if data != nil
        {
            let dataDict = JSON(data: data!)
            return dataDict
        }
        return JSON(integerLiteral:5)
    }
    
    func isConnectedToNetwork() -> Bool
    {
        var Status:Bool = false
        let url = NSURL(string: "http://google.com/")
        let request = NSURLRequest(URL: url!)
        let data = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
        
        if data != nil
        {
            Status = true
        }
        return Status
    }
    
    func CallPostApi(urlStr: String , postStr: String  ) -> JSON
    {
        print(postStr)

        let link = "http://pymu.winbrainindia.com/rest/"+urlStr
        let url = NSURL(string: link);
        let cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        let request = NSMutableURLRequest(URL: url!, cachePolicy: cachePolicy, timeoutInterval: 10.0)
        
        print("\(link)/?\(postStr)")
        request.HTTPMethod = "POST";
        request.HTTPBody = postStr.dataUsingEncoding(NSUTF8StringEncoding);
        
        if let data = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
        {
            let dataDict = JSON(data: data)
            print(dataDict)
            return dataDict
        }

        if self.isConnectedToNetwork() == false
        {
            return JSON(integerLiteral:6)
        }
        return JSON(integerLiteral:5)
    }
    
    func CallUpdatePicture(urlStr: String , parameters: [String: String]?, pic: UIImageView  ) -> JSON
    {
        let link = "http://pymu.winbrainindia.com/rest/"+urlStr
        let url = NSURL(string: link);
        let cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        let request = NSMutableURLRequest(URL: url!, cachePolicy: cachePolicy, timeoutInterval: 50.0)
        
        request.HTTPMethod = "POST"

        let boundary:String = "---------------------------14737809831466499882746641449"
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let body:NSMutableData = NSMutableData()
        
        if parameters != nil
        {
            for (key, value) in parameters!
            {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        if pic.image != nil
        {
            let img:UIImage = self.resizeImage(pic.image!)
            let imageData = UIImagePNGRepresentation(img)
            
            if imageData != nil
            {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"image\"; filename=\"image.png\"\r\n")
                body.appendString("Content-Type: image/png\r\n\r\n")
                body.appendData(imageData!)
                body.appendString("\r\n")
            }
        }
        
        body.appendString("--\(boundary)--\r\n")
        request.setValue("\(body.length)", forHTTPHeaderField:"Content-Length")
        request.HTTPBody = body

        if let data = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
        {
            let dataDict = JSON(data: data)
            print(dataDict)
            return dataDict
        }
        
        if self.isConnectedToNetwork() == false
        {
            return JSON(integerLiteral:6)
        }
        return JSON(integerLiteral:5)
    }

  
    /*
    func CallUpdatePicture(urlStr: String , pic: UIImageView  ) -> JSON
    {
        let link = "http://pymu.winbrainindia.com/rest/"+urlStr
        let url = NSURL(string: link);
        let cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        let request = NSMutableURLRequest(URL: url!, cachePolicy: cachePolicy, timeoutInterval: 50.0)
        
        request.HTTPMethod = "POST"
        let boundary:String = "---------------------------14737809831466499882746641449"
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let body:NSMutableData = NSMutableData()
        
        if pic.image != nil
        {
            let img:UIImage = self.resizeImage(pic.image!)
            let imageData = UIImagePNGRepresentation(img)
            
            if imageData != nil
            {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"image\"; filename=\"image.png\"\r\n")
                body.appendString("Content-Type: image/png\r\n\r\n")
                body.appendData(imageData!)
                body.appendString("\r\n")
            }
        }
        
        body.appendString("--\(boundary)--\r\n")
        request.setValue("\(body.length)", forHTTPHeaderField:"Content-Length")
        request.HTTPBody = body
        
        if let data = try? NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
        {
            let dataDict = JSON(data: data)
            print(dataDict)
            return dataDict
        }
        
        if self.isConnectedToNetwork() == false
        {
            return JSON(integerLiteral:6)
        }
        
        return JSON(integerLiteral:5)
    }*/
    
    func resizeImage(image:UIImage) -> UIImage
    {
        var actualHeight:Float = Float(image.size.height)
        var actualWidth:Float = Float(image.size.width)
        
        let maxHeight:Float = 180.0
        let maxWidth:Float = 180.0
        
        var imgRatio:Float = actualWidth/actualHeight
        let maxRatio:Float = maxWidth/maxHeight
        
        if (actualHeight > maxHeight) || (actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        let rect:CGRect = CGRectMake(0.0, 0.0, CGFloat(actualWidth) , CGFloat(actualHeight) )
        UIGraphicsBeginImageContext(rect.size)
        image.drawInRect(rect)
        
        let img:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        let imageData:NSData = UIImageJPEGRepresentation(img, 1.0)!
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData)!
    }
}
