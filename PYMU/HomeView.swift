//
//  HomeView.swift
//  PYMU
//
//  Created by Apple on 27/08/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit
class HomeView: UIViewController , UIAlertViewDelegate , UIGestureRecognizerDelegate , UITableViewDataSource , UITableViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var HomeView: UIView!
    @IBOutlet var Btn_menu: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    @IBOutlet var Btn_Next: UIButton!
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var BGImg_LatestBtn: UIImageView!
    @IBOutlet var Btn_LatestBet: UIButton!
    @IBOutlet var Btn_FriendBet: UIButton!
    
    @IBOutlet var LatestBetView: UIView!
    @IBOutlet var LatestBetTable: UITableView!
    
    @IBOutlet var FriendBetView: UIView!
    @IBOutlet var FriendBetTable: UITableView!
    
    var AllData:Array<AnyObject> = []
    var SelectRow:Int = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)
        self.HomeView.addSubview(Btn_menu)

        Btn_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        //self.revealViewController().rightRevealToggleAnimated(true)

        self.revealViewController().rearViewRevealWidth=(self.view.frame.width/5)*4
        self.revealViewController().rearViewRevealOverdraw=0
        self.revealViewController().rearViewRevealDisplacement=(self.view.frame.width/5)*4
        
        self.HomeViewDidLoad()
        FriendBetView.hidden=true
        self.LatestBet_Btn_Clicked(Btn_LatestBet)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        /*let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.fromValue = 0
        anim.toValue = M_PI
        anim.additive = true
        anim.duration = 0.4
        Btn_menu.layer.addAnimation(anim, forKey: "rotate")
        Btn_menu.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        */
        /*
        if Btn_menu.showsMenu
        {
            Btn_menu.showsMenu=false
        }
        else
        {
            Btn_menu.showsMenu=true
        }*/
        
        Btn_BackMenu.hidden=false
    }
 
    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        /*let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.fromValue = M_PI
        anim.toValue = 0
        anim.additive = true
        anim.duration = 0.4
        Btn_menu.layer.addAnimation(anim, forKey: "rotate")
        Btn_menu.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI))
        */
        /*
        if Btn_menu.showsMenu
        {
            Btn_menu.showsMenu=false
        }
        else
        {
            Btn_menu.showsMenu=true
        }*/
        
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    
    func setFrameX(frm:CGRect, x:CGFloat) -> CGRect
    {
        var frame=frm
        frame.origin.x = x
        return frame
    }

    
    func HomeViewDidLoad()
    {
        let LatestBetLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        LatestBetLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.LatestBetTable.addGestureRecognizer(LatestBetLeft)
        
        let LatestBetRight = UISwipeGestureRecognizer(target: self, action: #selector(self.NotRespond(_:)))
        LatestBetRight.direction = UISwipeGestureRecognizerDirection.Right
        self.LatestBetTable.addGestureRecognizer(LatestBetRight)
        
        let FriendBetLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.NotRespond(_:)))
        FriendBetLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.FriendBetTable.addGestureRecognizer(FriendBetLeft)
        
        let FriendBetRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(_:)))
        FriendBetRight.direction = UISwipeGestureRecognizerDirection.Right
        self.FriendBetTable.addGestureRecognizer(FriendBetRight)
    }
    
    func NotRespond(gesture: UIGestureRecognizer)
    {}
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case UISwipeGestureRecognizerDirection.Left:
                self.FriendBet_Btn_Clicked(Btn_FriendBet)
            case UISwipeGestureRecognizerDirection.Right:
                self.LatestBet_Btn_Clicked(Btn_LatestBet)
            default:
                break
            }
        }
    }
    
    @IBAction func Next_Btn_Clicked(sender: AnyObject)
    {
    }
    
    @IBAction func LatestBet_Btn_Clicked(sender: AnyObject)
    {
        LatestBetView.hidden=false
        UIView.animateWithDuration(0.4, animations:
            {
                self.LatestBetView.frame = self.setFrameX(self.LatestBetView.frame, x: 0)
                self.FriendBetView.frame = self.setFrameX(self.FriendBetView.frame, x: self.FriendBetView.frame.size.width)
                self.BGImg_LatestBtn.frame = self.setFrameX(self.BGImg_LatestBtn.frame , x: self.Btn_LatestBet.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.FriendBetView.hidden=true
        })
        
        Btn_LatestBet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_LatestBet.userInteractionEnabled=false
        Btn_LatestBet.tag=1
        Btn_FriendBet.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_FriendBet.userInteractionEnabled=true
        Btn_FriendBet.tag=0

        AllData=[]

        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("latestbets/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var invate:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
                    
                    invate["id"] = Int(arr["id"]!.string!)
                    invate["user1"] = arr["user1"]!.string!
                    invate["user2"] = arr["user2"]!.string!
                    
                    invate["tournament_type"] = arr["tournament_type"]!.string!
                    invate["tournament_name"] = arr["tournament_name"]!.string!
                    invate["tournament_image"] = arr["image"]!.string!
                    
                    invate["duration"] = arr["duration"]!.string!

                    invate["commission"] = arr["commission"]!.string!
                    invate["schedule_on"]=arr["schedule_on"]!.string!
                    invate["bet_type"] = arr["category_type"]!.string!
                    invate["totalbets"] = arr["totalbets"]!.string!

                    var teams = arr["artists"]!
                    var artists:Array<AnyObject> = Array<AnyObject>()
                    for art in 0  ..< teams.count 
                    {
                        var team=teams[art].dictionaryValue
                        
                        var tem:Array<String> = Array<String>()
                        tem.append(team["artist_id"]!.string!)
                        tem.append(team["artist_name"]!.string!)
                        tem.append(team["image"]!.string!)
                        
                        artists.append(tem)
                    }
                    invate["artists"]=artists
                    
                    AllData.append(invate)
                }
            }
        }
        LatestBetTable.reloadData()
    }
    
    @IBAction func FriendBet_Btn_Clicked(sender: AnyObject)
    {
        FriendBetView.hidden=false
        self.FriendBetView.frame = self.setFrameX(self.FriendBetView.frame, x: self.FriendBetView.frame.size.width)
        self.LatestBetView.frame = self.setFrameX(self.LatestBetView.frame, x: 0)
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.LatestBetView.frame = self.setFrameX(self.LatestBetView.frame, x: -(self.LatestBetView.frame.size.width))
                self.FriendBetView.frame = self.setFrameX(self.FriendBetView.frame, x: 0)
                self.BGImg_LatestBtn.frame = self.setFrameX(self.BGImg_LatestBtn.frame , x: self.Btn_FriendBet.frame.origin.x)

            },
            completion:
            {(value: Bool) in
                self.LatestBetView.hidden=true
        })
        
        Btn_FriendBet.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_LatestBet.userInteractionEnabled=true
        Btn_LatestBet.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_FriendBet.userInteractionEnabled=false
        Btn_LatestBet.tag=0
        Btn_FriendBet.tag=1
        AllData=[]

        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("friendsbet/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var invate:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
                    
                    invate["id"] = Int(arr["id"]!.string!)
                    invate["user1"] = arr["user1"]!.string!
                    invate["user2"] = arr["user2"]!.string!
                    
                    invate["tournament_type"] = arr["tournament_type"]!.string!
                    invate["tournament_name"] = arr["tournament_name"]!.string!
                    invate["tournament_image"] = arr["image"]!.string!
                    
                    invate["duration"] = arr["duration"]!.string!

                    invate["commission"] = arr["commission"]!.string!
                    invate["schedule_on"]=arr["schedule_on"]!.string!
                    invate["bet_type"] = arr["category_type"]!.string!
                    invate["totalbets"] = arr["totalbets"]!.string!

                    var teams = arr["artists"]!
                    var artists:Array<AnyObject> = Array<AnyObject>()
                    for art in 0  ..< teams.count 
                    {
                        var team=teams[art].dictionaryValue
                        
                        var tem:Array<String> = Array<String>()
                        tem.append(team["artist_id"]!.string!)
                        tem.append(team["artist_name"]!.string!)
                        tem.append(team["image"]!.string!)
                        
                        artists.append(tem)
                    }
                    invate["artists"]=artists
                    
                    AllData.append(invate)
                }
            }
        }
        FriendBetTable.reloadData()
    }

    
    // All Table Methods....
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return AllData.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var Live:Dictionary<String,AnyObject> = AllData[indexPath.section] as! Dictionary<String, AnyObject>

        if Live["tournament_type"] as! String == "Tournament"
        {
            let cell: TBetListCell! = tableView.dequeueReusableCellWithIdentifier("TBetListCell") as? TBetListCell
            
            cell.Team1.setImageWithUrl(NSURL(string:Live["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
            cell.Team1.layer.cornerRadius=19
            
            let user1=Live["user1"]!.componentsSeparatedByString(" ")
            let user2=Live["user2"]!.componentsSeparatedByString(" ")

            let name = "\(user1[0]) \(user1[1][user1[1].startIndex]). Bet \(user2[0]) \(user2[1][user2[1].startIndex])."
            let range: NSRange = (name as NSString).rangeOfString("Bet")
            
            let Bet = NSMutableAttributedString(string: name)
            Bet.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 179.0/255.0, green: 0.0/255.0, blue: 19.0/255.0, alpha: 1.0), range: range)
            Bet.addAttribute(NSFontAttributeName, value: cell.NameUser.font.fontWithSize(11), range: range)
            
            cell.NameUser.attributedText = Bet
            cell.BetTime.text=Live["duration"]! as? String
            cell.NumBetor.text=Live["totalbets"]! as? String
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.blackColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()
            
            return cell!
        }
        else if Live["tournament_type"] as! String == "Match"
        {
            let cell: BetListCell! = tableView.dequeueReusableCellWithIdentifier("BetListCell") as? BetListCell
            
            var art1:Array<String> = Live["artists"]!.objectAtIndex(0) as! Array<String>
            var art2:Array<String> = Live["artists"]!.objectAtIndex(1) as! Array<String>
            
            cell.Team1.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
            cell.Team2.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
            
            cell.Team1.layer.cornerRadius=19
            cell.Team2.layer.cornerRadius=19
            
            let user1=Live["user1"]!.componentsSeparatedByString(" ")
            let user2=Live["user2"]!.componentsSeparatedByString(" ")
            
            let name = "\(user1[0]) \(user1[1][user1[1].startIndex]). Bet \(user2[0]) \(user2[1][user2[1].startIndex])."
            let range: NSRange = (name as NSString).rangeOfString("Bet")
            
            let Bet = NSMutableAttributedString(string: name)
            Bet.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 179.0/255.0, green: 0.0/255.0, blue: 19.0/255.0, alpha: 1.0), range: range)
            Bet.addAttribute(NSFontAttributeName, value: cell.NameUser.font.fontWithSize(11), range: range)
            
            cell.NameUser.attributedText = Bet
            cell.BetTime.text=Live["duration"]! as? String
            cell.NumBetor.text=Live["totalbets"]! as? String
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.blackColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()
            
            return cell!
        }
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        SelectRow=indexPath.section

        self.performSegueWithIdentifier("HomeToSelectUser", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "HomeToSelectUser"
        {
            let secondViewController = (segue.destinationViewController as! SelectUser)
            NSUserDefaults.standardUserDefaults().setObject("Home" , forKey: "BettingView")
            secondViewController.LiveData=AllData[SelectRow] as! Dictionary<String, AnyObject>
        }
    }
}
