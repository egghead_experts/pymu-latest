//
//  DisplayTournament.swift
//  PYMU
//
//  Created by Egghead IOS3 on 10/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit
import coinbase_official

class DisplayTournament: UIViewController, UIGestureRecognizerDelegate , UIAlertViewDelegate, UIScrollViewDelegate, UICollectionViewDataSource , UICollectionViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var GamePlace: UILabel!
    
    @IBOutlet var SelectionView: UIView!
    @IBOutlet var collection: UICollectionView!
    
    @IBOutlet var BetView: UIView!
    @IBOutlet var WON_Btn: UIButton!
    @IBOutlet var LOSE_Btn: UIButton!
    @IBOutlet var LoseBet: UILabel!
    
    @IBOutlet var Btn_PYMU: UIButton!
    
    @IBOutlet var PopupView: UIView!
    @IBOutlet var OpenBetAlert: UIView!
    @IBOutlet var OpenBetMessage: UILabel!
    @IBOutlet var InviteAlert: UIView!
    @IBOutlet var InviteMessage: UILabel!
    @IBOutlet var BetAmmountLbl: UILabel!

    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    
    var LiveData:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
    
    var Bet_Id:Int = 0
    var Artist_Id:String = ""

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setViewData()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DisplayTournament.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
    }
    
    
    func setViewData()
    {
        GamePlace.text=LiveData["tournament_name"]! as? String
        
        let layout:UICollectionViewFlowLayout=UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing=0.0
        layout.minimumLineSpacing=0.0
        layout.scrollDirection=UICollectionViewScrollDirection.Horizontal
        collection.collectionViewLayout=layout
        
        Artist_Id=LiveData["artist_id"] as! String
        
        LoseBet.text=LiveData["bet_amount"]! as? String
        
        if LiveData["bet_amount"]! as? String == "win"
        {
            WON_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
            LOSE_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)        }
        else
        {
            WON_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            LOSE_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        }
        
        PopupView.hidden=true
        contentView.bringSubviewToFront(PopupView)
    }
    
    @IBAction func BackBtn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (LiveData["artists"]?.count)!
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell: UICollectionViewCell=collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath)
        
        var art:Array<String> = LiveData["artists"]!.objectAtIndex(indexPath.row) as! Array<String>
        
        let teamImg:UIImageView = cell.viewWithTag(11) as! UIImageView
        teamImg.setImageWithUrl(NSURL(string:art[2])!, placeHolderImage: UIImage(named:"team_bg"))
        teamImg.layer.cornerRadius=30
        teamImg.layer.masksToBounds = true
        
        let teamFrm:UIImageView = cell.viewWithTag(22) as! UIImageView
        if Artist_Id == art[0]
        {
            teamFrm.image=UIImage(named: "team_frm_in_sel.png")
        }
        else
        {
            teamFrm.image=UIImage(named: "team_frm_in.png")
        }
        
        let teamName:UILabel = cell.viewWithTag(33) as! UILabel
        teamName.text=art[1]
        
        cell.contentView.frame = cell.bounds
        cell.backgroundColor=UIColor.clearColor()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {}
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width: self.SelectionView.frame.width/2.5 , height: self.SelectionView.frame.height/2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    // --------------------
    //   *** Bet View ***
    // --------------------
    
    @IBAction func PYMU_Btn_Clicked(sender: AnyObject)
    {
        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
        {
            UIView.animateWithDuration(0.2, animations:
                {
                    self.PopupView.hidden=false
                    self.InviteAlert.layer.masksToBounds=true
                    self.InviteAlert.hidden=false
                    self.InviteMessage.text="Are you sure want to bet with \(self.LiveData["user_name"]!)?"
                    self.BetAmmountLbl.text="$\(self.LiveData["bet_amount"]!)"
                    self.OpenBetAlert.hidden=true
                    self.PopupView.alpha=0.7
                    self.PopupView.alpha=1.0
                },
                completion:
                {(value: Bool) in
            })
        }
        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
        {
            UIView.animateWithDuration(0.2, animations:
                {
                    self.PopupView.hidden=false
                    self.OpenBetAlert.layer.masksToBounds=true
                    self.OpenBetMessage.text="Are you sure want to bet with \(self.LiveData["user_name"]!)?"
                    self.OpenBetAlert.hidden=false
                    self.InviteAlert.hidden=true
                    self.PopupView.alpha=0.7
                    self.PopupView.alpha=1.0
                },
                completion:
                {(value: Bool) in
            })
        }
    }
    
    @IBAction func Invite_Accept_Clicked(sender: AnyObject)
    {
        CoinbaseOAuth.startOAuthAuthenticationWithClientId(CLIENT_ID , scope: "user balance", redirectUri: "com.egghead.pymu.coinbase-oauth://coinbase-oauth", meta: nil)
        NSUserDefaults.standardUserDefaults().setObject("TournamentBetInvite" , forKey: "coinbase")

    }
    
    func authenticationComplete(response:[String : AnyObject])
    {
        Bet_Id=Int(LiveData["bet_id"] as! String)!
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "bet_id=\(Bet_Id)&response=accept"
        
        var dataDict=ApiObj.CallPostApi("user/acceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    },
                                           completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    @IBAction func Invite_Decline_Clicked(sender: AnyObject)
    {
        Bet_Id=Int(LiveData["bet_id"] as! String)!
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "bet_id=\(Bet_Id)&response=declined"
        
        var dataDict=ApiObj.CallPostApi("user/acceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    
    @IBAction func Open_Bet_Accept_Clicked(sender: AnyObject)
    {
        CoinbaseOAuth.startOAuthAuthenticationWithClientId(CLIENT_ID , scope: "user balance", redirectUri: "com.egghead.pymu.coinbase-oauth://coinbase-oauth", meta: nil)

        NSUserDefaults.standardUserDefaults().setObject("TournamentBetOpen" , forKey: "coinbase")

    }
    
    func authenticationComplete1(response:[String : AnyObject])
    {
        Bet_Id=Int(LiveData["bet_id"] as! String)!
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "bet_id=\(Bet_Id)&response=accept"
        
        var dataDict=ApiObj.CallPostApi("user/acceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    },
                                           completion:
                    {(value: Bool) in
                })
            }
        }        
    }
    
    @IBAction func Open_Bet_Cancel_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopupView.hidden=true
            },
            completion:
            {(value: Bool) in
        })
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
