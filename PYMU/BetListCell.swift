//
//  BetListCell.swift
//  PYMU
//
//  Created by Apple on 19/08/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class BetListCell: UITableViewCell
{
    @IBOutlet var Team1: UIImageView!
    @IBOutlet var Team1Frm: UIImageView!
    @IBOutlet var Team2: UIImageView!
    @IBOutlet var Team2Frm: UIImageView!
    @IBOutlet var vsImg: UIImageView!
    
    @IBOutlet var NameUser: UILabel!
    @IBOutlet var BetTime: UILabel!
    @IBOutlet var NumBetor: UILabel!

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
