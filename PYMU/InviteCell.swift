//
//  InviteCell.swift
//  PYMU
//
//  Created by Apple on 29/07/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class InviteCell: UITableViewCell
{
    @IBOutlet var Team1: UIImageView!
    @IBOutlet var Team1Frm: UIImageView!
    @IBOutlet var Team2: UIImageView!
    @IBOutlet var Team2Frm: UIImageView!
    @IBOutlet var vsImg: UIImageView!
    @IBOutlet var NextImg: UIImageView!
    
    
    @IBOutlet var NameUser: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
