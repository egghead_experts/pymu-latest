//
//  MatchBet.swift
//  PYMU
//
//  Created by Egghead IOS3 on 08/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit
import coinbase_official

class MatchBet: UIViewController, UIGestureRecognizerDelegate , UIAlertViewDelegate, UIScrollViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var Btn_Back: UIButton!
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var GamePlace: UILabel!
    
    @IBOutlet var Team1_Img: UIImageView!
    @IBOutlet var Team1_Frm: UIImageView!
    @IBOutlet var Team2_Img: UIImageView!
    @IBOutlet var Team2_Frm: UIImageView!
    @IBOutlet var Team1_Name: UILabel!
    @IBOutlet var Team2_Name: UILabel!
    @IBOutlet var Btn_Team1: UIButton!
    @IBOutlet var Btn_Team2: UIButton!
    
    @IBOutlet var BetView: UIView!
    
    var timer = NSTimer()
    var SP_UP_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var SB_UP_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var LB_UP_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var OP_UP_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var OB_UP_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    
    var SP_DOWN_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var SB_DOWN_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var LB_DOWN_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var OP_DOWN_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    var OB_DOWN_longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    
    
    @IBOutlet var Btn_Spread: UIButton!
    @IBOutlet var SpreadPoints: UILabel!
    @IBOutlet var Btn_SP_UP: UIButton!
    @IBOutlet var Btn_SP_DOWN: UIButton!
    @IBOutlet var SpreadBet: UILabel!
    @IBOutlet var Btn_SB_UP: UIButton!
    @IBOutlet var Btn_SB_DOWN: UIButton!
    
    @IBOutlet var Btn_Lose: UIButton!
    @IBOutlet var WON_Btn: UIButton!
    @IBOutlet var LOSE_Btn: UIButton!
    @IBOutlet var LoseBet: UILabel!
    @IBOutlet var Btn_LB_UP: UIButton!
    @IBOutlet var Btn_LB_DOWN: UIButton!
    
    
    @IBOutlet var Btn_Over: UIButton!
    @IBOutlet var OVER_Btn: UIButton!
    @IBOutlet var UNDER_Btn: UIButton!
    @IBOutlet var OverPoints: UILabel!
    @IBOutlet var Btn_OP_UP: UIButton!
    @IBOutlet var Btn_OP_DOWN: UIButton!
    @IBOutlet var OverBet: UILabel!
    @IBOutlet var Btn_OB_UP: UIButton!
    @IBOutlet var Btn_OB_DOWN: UIButton!
    
  
    @IBOutlet var Btn_PYMU: UIButton!
    
    @IBOutlet var PYMUBtn_BottomSpace: NSLayoutConstraint!
    @IBOutlet var WinLoseBtn_TopSpace: NSLayoutConstraint!
    @IBOutlet var SpreadBtn_TopSpace: NSLayoutConstraint!
    @IBOutlet var OverBtn_TopSpace: NSLayoutConstraint!
    
    
    @IBOutlet var PopupView: UIView!
    @IBOutlet var AlertView: UIView!
    @IBOutlet var AlertMessage: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    
    var LiveData:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
    var BetType:[String] = []
    
    var Tournament_Id:Int = 0
    var User_Id:Int = 0
    var User_Name:String = ""
    var Artist_Id:Int = -1
    var Bet_For:String = ""
    var Point:Int = 0
    var Amount:Int = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setViewData()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MatchBet.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
    }
    
    
    func setViewData()
    {
        GamePlace.text=LiveData["tournament_name"]! as? String
        self.TitleLbl.text="Bet"

        var art1:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
        var art2:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
        
        Team1_Name.text=art1[1]
        Team2_Name.text=art2[1]
        
        Team1_Img.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
        Team2_Img.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
        
        Team1_Img.layer.cornerRadius=27
        Team1_Img.layer.masksToBounds = true
        Team2_Img.layer.cornerRadius=27
        Team2_Img.layer.masksToBounds = true
                
        BetType=["0"]
        self.AllBtnShow()
        
        let typeStr:String = LiveData["bet_type"] as! String
        BetType=typeStr.componentsSeparatedByString(",")
        self.AllBtnShow()
        
        self.WIN_Btn_Clicked(WON_Btn)
        self.OVER_Btn_Clicked(OVER_Btn)
        
        PopupView.hidden=true
        contentView.bringSubviewToFront(PopupView)

        Btn_Team1.userInteractionEnabled=true
        Btn_Team2.userInteractionEnabled=true
        
        
        SP_UP_longPress.addTarget(self, action: #selector(MatchBet.UP_longPressed(_:)))
        Btn_SP_UP.addGestureRecognizer(SP_UP_longPress)
        
        SB_UP_longPress.addTarget(self, action: #selector(MatchBet.UP_longPressed(_:)))
        Btn_SB_UP.addGestureRecognizer(SB_UP_longPress)
        
        LB_UP_longPress.addTarget(self, action: #selector(MatchBet.UP_longPressed(_:)))
        Btn_LB_UP.addGestureRecognizer(LB_UP_longPress)
        
        OP_UP_longPress.addTarget(self, action: #selector(MatchBet.UP_longPressed(_:)))
        Btn_OP_UP.addGestureRecognizer(OP_UP_longPress)
        
        OB_UP_longPress.addTarget(self, action: #selector(MatchBet.UP_longPressed(_:)))
        Btn_OB_UP.addGestureRecognizer(OB_UP_longPress)
        
        
        SP_DOWN_longPress.addTarget(self, action: #selector(MatchBet.DOWN_longPressed(_:)))
        Btn_SP_DOWN.addGestureRecognizer(SP_DOWN_longPress)
        
        SB_DOWN_longPress.addTarget(self, action: #selector(MatchBet.DOWN_longPressed(_:)))
        Btn_SB_DOWN.addGestureRecognizer(SB_DOWN_longPress)
        
        LB_DOWN_longPress.addTarget(self, action: #selector(MatchBet.DOWN_longPressed(_:)))
        Btn_LB_DOWN.addGestureRecognizer(LB_DOWN_longPress)
        
        OP_DOWN_longPress.addTarget(self, action: #selector(MatchBet.DOWN_longPressed(_:)))
        Btn_OP_DOWN.addGestureRecognizer(OP_DOWN_longPress)
        
        OB_DOWN_longPress.addTarget(self, action: #selector(MatchBet.DOWN_longPressed(_:)))
        Btn_OB_DOWN.addGestureRecognizer(OB_DOWN_longPress)
        
        
        if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            SpreadBtn_TopSpace.constant=5
            WinLoseBtn_TopSpace.constant=5
            OverBtn_TopSpace.constant=5
            
            PYMUBtn_BottomSpace.constant=5
        }
        else if UIScreen.mainScreen().bounds.size.height == 320.0 && UIScreen.mainScreen().bounds.size.width == 480.0
        {
            SpreadBtn_TopSpace.constant=5
            WinLoseBtn_TopSpace.constant=5
            OverBtn_TopSpace.constant=5
            
            PYMUBtn_BottomSpace.constant=5
        }
        else
        {
            SpreadBtn_TopSpace.constant=25
            WinLoseBtn_TopSpace.constant=15
            OverBtn_TopSpace.constant=15
            
            PYMUBtn_BottomSpace.constant=15
        }
    }
    
    
    @IBAction func BackBtn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func Team1_Selecation_Clicked(sender: AnyObject)
    {
        Team1_Frm.image=UIImage(named: "team_frm_in_sel.png")
        Team2_Frm.image=UIImage(named: "team_frm_in.png")
        
        var art:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
        Artist_Id=Int(art[0])!
    }
    
    @IBAction func Team2_Selection_Clicked(sender: AnyObject)
    {
        Team1_Frm.image=UIImage(named: "team_frm_in.png")
        Team2_Frm.image=UIImage(named: "team_frm_in_sel.png")
        
        var art:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
        Artist_Id=Int(art[0])!
    }
    
    // --------------------
    //   *** Bet View ***
    // --------------------
    
    func AllBtnShow()
    {
        for i in BetType
        {
            if i == "1"
            {
                Btn_Spread.setImage(UIImage(named: "btn_spread.png"), forState: UIControlState.Normal)
                Btn_Spread.tag=0
                Btn_Spread.userInteractionEnabled=true
                BetView.bringSubviewToFront(Btn_Spread)
            }
            else if i == "2"
            {
                Btn_Lose.setImage(UIImage(named: "btn_lose.png"), forState: UIControlState.Normal)
                Btn_Lose.tag=0
                Btn_Lose.userInteractionEnabled=true
                BetView.bringSubviewToFront(Btn_Lose)
            }
            else if i == "3"
            {
                Btn_Over.setImage(UIImage(named: "btn_over.png"), forState: UIControlState.Normal)
                Btn_Over.tag=0
                Btn_Over.userInteractionEnabled=true
                BetView.bringSubviewToFront(Btn_Over)
            }
            else
            {
                Btn_Spread.setImage(UIImage(named: "btn_spread.png"), forState: UIControlState.Normal)
                Btn_Lose.setImage(UIImage(named: "btn_lose.png"), forState: UIControlState.Normal)
                Btn_Over.setImage(UIImage(named: "btn_over.png"), forState: UIControlState.Normal)
                
                Btn_Spread.tag=0
                Btn_Lose.tag=0
                Btn_Over.tag=0
                
                Btn_Spread.userInteractionEnabled=false
                Btn_Lose.userInteractionEnabled=false
                Btn_Over.userInteractionEnabled=false
                
                BetView.bringSubviewToFront(Btn_Spread)
                BetView.bringSubviewToFront(Btn_Lose)
                BetView.bringSubviewToFront(Btn_Over)
                
                SpreadPoints.text="0"
                SpreadBet.text="0"
                LoseBet.text="0"
                OverPoints.text="0"
                OverBet.text="0"
            }
        }
    }
    
    @IBAction func Spread_Btn_Clicked(sender: AnyObject)
    {
        self.AllBtnShow()
        
        Btn_Spread.setImage(UIImage(named: "bat_spred_bg.png"), forState: UIControlState.Normal)
        Btn_Spread.tag=1
        Btn_Spread.userInteractionEnabled=false
        BetView.sendSubviewToBack(Btn_Spread)
    }
    
    @IBAction func Lose_Btn_Clicked(sender: AnyObject)
    {
        self.AllBtnShow()
        
        Btn_Lose.setImage(UIImage(named: "bat_lose_bg.png"), forState: UIControlState.Normal)
        Btn_Lose.tag=1
        Btn_Lose.userInteractionEnabled=false
        BetView.sendSubviewToBack(Btn_Lose)
    }
    
    @IBAction func WIN_Btn_Clicked(sender: AnyObject)
    {
        WON_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        LOSE_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        WON_Btn.userInteractionEnabled=false
        LOSE_Btn.userInteractionEnabled=true
        WON_Btn.tag=1
        LOSE_Btn.tag=0
    }
    @IBAction func LOSE_Btn_Clicked(sender: AnyObject)
    {
        WON_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        LOSE_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        WON_Btn.userInteractionEnabled=true
        LOSE_Btn.userInteractionEnabled=false
        WON_Btn.tag=0
        LOSE_Btn.tag=1
    }
    
    @IBAction func Over_Btn_Clicked(sender: AnyObject)
    {
        self.AllBtnShow()
        
        Btn_Over.setImage(UIImage(named: "bat_over_bg.png"), forState: UIControlState.Normal)
        Btn_Over.tag=1
        Btn_Over.userInteractionEnabled=false
        BetView.sendSubviewToBack(Btn_Over)
    }
    
    @IBAction func OVER_Btn_Clicked(sender: AnyObject)
    {
        OVER_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        UNDER_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        OVER_Btn.userInteractionEnabled=false
        UNDER_Btn.userInteractionEnabled=true
        OVER_Btn.tag=1
        UNDER_Btn.tag=0
    }
    @IBAction func UNDER_Btn_Clicked(sender: AnyObject)
    {
        UNDER_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        OVER_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        OVER_Btn.userInteractionEnabled=true
        UNDER_Btn.userInteractionEnabled=false
        OVER_Btn.tag=0
        UNDER_Btn.tag=1
    }
    
    @IBAction func Btn_UP_Clicked(sender: AnyObject)
    {
        let myBtn: AnyObject=sender
        
        if myBtn.isEqual(Btn_SP_UP)
        {
            SpreadPoints.text="\(Int(SpreadPoints.text!)! + 1)"
        }
        else if myBtn.isEqual(Btn_SB_UP)
        {
            //let fa:Float=NSString(string: SpreadBet.text!).floatValue
            //SpreadBet.text="\(fa + 0.1)"
            SpreadBet.text="\(Int(SpreadBet.text!)! + 1)"

        }
        else if myBtn.isEqual(Btn_LB_UP)
        {
            LoseBet.text="\(Int(LoseBet.text!)! + 1)"
        }
        else if myBtn.isEqual(Btn_OP_UP)
        {
            OverPoints.text="\(Int(OverPoints.text!)! + 1)"
        }
        else if myBtn.isEqual(Btn_OB_UP)
        {
            OverBet.text="\(Int(OverBet.text!)! + 1)"
        }
    }
    
    func UP_longPressed(sender: UILongPressGestureRecognizer)
    {
        var i:Int = 0
        
        if sender.isEqual(SP_UP_longPress)
        {
            i = 1
        }
        else if sender.isEqual(SB_UP_longPress)
        {
            i = 2
        }
        else if sender.isEqual(LB_UP_longPress)
        {
            i = 3
        }
        else if sender.isEqual(OP_UP_longPress)
        {
            i = 4
        }
        else if sender.isEqual(OB_UP_longPress)
        {
            i = 5
        }
        
        if ( sender.state == UIGestureRecognizerState.Began )
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(MatchBet.UpAction), userInfo: i, repeats: true)
        }
        if ( sender.state == UIGestureRecognizerState.Ended )
        {
            timer.invalidate()
        }
    }
    
    func UpAction()
    {
        let i:Int = timer.userInfo as! Int
        
        if i == 1
        {
            SpreadPoints.text="\(Int(SpreadPoints.text!)! + 1)"
        }
        else if i == 2
        {
            //let fa:Float=NSString(string: SpreadBet.text!).floatValue
            //SpreadBet.text="\(fa + 0.1)"
            SpreadBet.text="\(Int(SpreadBet.text!)! + 1)"
        }
        else if i == 3
        {
            LoseBet.text="\(Int(LoseBet.text!)! + 1)"
        }
        else if i == 4
        {
            OverPoints.text="\(Int(OverPoints.text!)! + 1)"
        }
        else if i == 5
        {
            OverBet.text="\(Int(OverBet.text!)! + 1)"
        }
    }
    
    @IBAction func Btn_DOWN_Clicked(sender: AnyObject)
    {
        let myBtn: AnyObject=sender
        
        if myBtn.isEqual(Btn_SP_DOWN) && Int(SpreadPoints.text!) > 0
        {
            SpreadPoints.text="\(Int(SpreadPoints.text!)! - 1)"
        }
        else if myBtn.isEqual(Btn_SB_DOWN) && Int(SpreadBet.text!) > 0
        {
            //let fa:Float=NSString(string: SpreadBet.text!).floatValue
            //SpreadBet.text="\(fa - 0.1)"
            SpreadBet.text="\(Int(SpreadBet.text!)! - 1)"

        }
        else if myBtn.isEqual(Btn_LB_DOWN) && Int(LoseBet.text!) > 0
        {
            LoseBet.text="\(Int(LoseBet.text!)! - 1)"
        }
        else if myBtn.isEqual(Btn_OP_DOWN) && Int(OverPoints.text!) > 0
        {
            OverPoints.text="\(Int(OverPoints.text!)! - 1)"
        }
        else if myBtn.isEqual(Btn_OB_DOWN) && Int(OverBet.text!) > 0
        {
            OverBet.text="\(Int(OverBet.text!)! - 1)"
        }
    }
    
    func DOWN_longPressed(sender: UILongPressGestureRecognizer)
    {
        var i:Int = 0
        
        if sender.isEqual(SP_DOWN_longPress)
        {
            i = 1
        }
        else if sender.isEqual(SB_DOWN_longPress)
        {
            i = 2
        }
        else if sender.isEqual(LB_DOWN_longPress)
        {
            i = 3
        }
        else if sender.isEqual(OP_DOWN_longPress)
        {
            i = 4
        }
        else if sender.isEqual(OB_DOWN_longPress)
        {
            i = 5
        }
        
        if ( sender.state == UIGestureRecognizerState.Began )
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(MatchBet.DownAction), userInfo: i, repeats: true)
        }
        if ( sender.state == UIGestureRecognizerState.Ended )
        {
            timer.invalidate()
        }
    }
    
    func DownAction()
    {
        let i:Int = timer.userInfo as! Int
        
        if i == 1 && Int(SpreadPoints.text!) > 0
        {
            SpreadPoints.text="\(Int(SpreadPoints.text!)! - 1)"
        }
        else if i == 2 && Int(SpreadBet.text!) > 0
        {
            //let fa:Float=NSString(string: SpreadBet.text!).floatValue
            //SpreadBet.text="\(fa - 0.1)"
            SpreadBet.text="\(Int(SpreadBet.text!)! - 1)"
        }
        else if i == 3 && Int(LoseBet.text!) > 0
        {
            LoseBet.text="\(Int(LoseBet.text!)! - 1)"
        }
        else if i == 4 && Int(OverPoints.text!) > 0
        {
            OverPoints.text="\(Int(OverPoints.text!)! - 1)"
        }
        else if i == 5 && Int(OverBet.text!) > 0
        {
            OverBet.text="\(Int(OverBet.text!)! - 1)"
        }
    }
    
    @IBAction func PYMU_Btn_Clicked(sender: AnyObject)
    {
        Bet_For = ""
        Point = 0
        Amount = 0
        
        var msg:String = ""
        
        if Artist_Id == -1
        {
            msg = "Please Select any Team/Player"
        }
        else if Btn_Spread.tag == 0 && Btn_Lose.tag == 0 && Btn_Over.tag == 0
        {
            msg = "Please Select any Bet Type"
        }
        else if Btn_Spread.tag == 1
        {
            if Int(SpreadPoints.text!) <= 0
            {
                msg = "Enter valid Point"
            }
            else if Int(SpreadBet.text!) <= 0
            {
                msg = "Enter valid Bet amount"
            }
            else
            {
                Bet_For="spread"
                Point=Int(SpreadPoints.text!)!
                Amount=Int(SpreadBet.text!)!
            }
        }
        else if Btn_Lose.tag == 1
        {
            if Int(LoseBet.text!) <= 0
            {
                msg = "Enter valid Bet amount"
            }
            else
            {
                if WON_Btn.tag == 1
                {
                    Bet_For="win"
                }
                else if LOSE_Btn.tag == 1
                {
                    Bet_For="lose"
                }
                
                Point=0
                Amount=Int(LoseBet.text!)!
            }
        }
        else if Btn_Over.tag == 1
        {
            if Int(OverPoints.text!) <= 0
            {
                msg = "Enter valid Point"
            }
            else if Int(OverBet.text!) <= 0
            {
                msg = "Enter valid Bet amount"
            }
            else
            {
                if OVER_Btn.tag == 1
                {
                    Bet_For="over"
                }
                else if UNDER_Btn.tag == 1
                {
                    Bet_For="under"
                }
                
                Point=Int(OverPoints.text!)!
                Amount=Int(OverBet.text!)!
            }
        }
        
        
        if msg == ""
        {
            UIView.animateWithDuration(0.2, animations:
                {
                    self.PopupView.hidden=false
                    self.AlertView.layer.masksToBounds=true
                    self.AlertMessage.text="Are you sure want to bet with \(self.User_Name)?"
                    self.PopupView.alpha=0.7
                    self.PopupView.alpha=1.0
                },
                completion:
                {(value: Bool) in
            })
        }
        else
        {
            let alertController = UIAlertController(title: "PYMU", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func Alert_Bet_Clicked(sender: AnyObject)
    {
        CoinbaseOAuth.startOAuthAuthenticationWithClientId(CLIENT_ID , scope: "user balance", redirectUri: "com.egghead.pymu.coinbase-oauth://coinbase-oauth", meta: nil)
        
        NSUserDefaults.standardUserDefaults().setObject("MatchBet" , forKey: "coinbase")

    }
    
    func authenticationComplete(response:[String : AnyObject])
    {
        print(LiveData)
        Tournament_Id=LiveData["id"] as! Int
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "tournament_id=\(Tournament_Id)&artist_id=\(Artist_Id)&user_id=\(User_Id)&bet_for=\(Bet_For)&points=\(Point)&bet_amount=\(Amount)"
        
        var dataDict=ApiObj.CallPostApi("user/bet/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        
                        if (NSUserDefaults.standardUserDefaults().objectForKey("BettingView") as! String) == "Home"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("BettingView") as! String) == "SelectMatch"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
                        }
                    },
                                           completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    
    @IBAction func Alert_Cancel_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopupView.hidden=true
            },
            completion:
            {(value: Bool) in
        })
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
