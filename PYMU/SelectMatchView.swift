//
//  SelectMatchView.swift
//  PYMU
//
//  Created by Apple on 04/09/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class SelectMatchView: UIViewController
{
    var ApiObj = PymuAPI()

    @IBOutlet var BGImg_LiveBtn: UIImageView!
    @IBOutlet var Btn_Live: UIButton!
    @IBOutlet var Btn_Upcoming: UIButton!
    
    @IBOutlet var LiveView: UIView!
    @IBOutlet var LiveTable: UITableView!
    
    @IBOutlet var UpcomingView: UIView!
    @IBOutlet var UpcomingTable: UITableView!
    
    var selectLiveCategory = 0
    var LiveCategory:Array<String> = []
    var NumLiveMatch:Array<Int> = []
    var LiveMatchId:Array<Array<Int>> = []
    var LiveTournament:Dictionary<Int, AnyObject> = Dictionary<Int, AnyObject>()

    var selectUpcomeCategory = 0
    var UpcomeCategory:Array<String> = []
    var NumUpcomeMatch:Array<Int> = []
    var UpcomeMatchId:Array<Array<Int>> = []
    var UpcomeTournament:Dictionary<Int, AnyObject> = Dictionary<Int, AnyObject>()

    var SelectSection:Int = 0
    var SelectRow:Int = 0


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        UpcomingView.hidden=true
        self.Live_Btn_Clicked(Btn_Live)
        
        let LiveLeft = UISwipeGestureRecognizer(target: self, action: #selector(SelectMatchView.respondToSwipeGesture(_:)))
        LiveLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.LiveTable.addGestureRecognizer(LiveLeft)
        
        let LiveRight = UISwipeGestureRecognizer(target: self, action: #selector(SelectMatchView.NotRespond(_:)))
        LiveRight.direction = UISwipeGestureRecognizerDirection.Right
        self.LiveTable.addGestureRecognizer(LiveRight)
        
        let UpcomingLeft = UISwipeGestureRecognizer(target: self, action: #selector(SelectMatchView.NotRespond(_:)))
        UpcomingLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.UpcomingTable.addGestureRecognizer(UpcomingLeft)
        
        let UpcomingRight = UISwipeGestureRecognizer(target: self, action: #selector(SelectMatchView.respondToSwipeGesture(_:)))
        UpcomingRight.direction = UISwipeGestureRecognizerDirection.Right
        self.UpcomingTable.addGestureRecognizer(UpcomingRight)
    }
    
    func setFrameX(frm:CGRect, x:CGFloat) -> CGRect
    {
        var frame=frm
        frame.origin.x = x
        return frame
    }
    
    func NotRespond(gesture: UIGestureRecognizer)
    {}

    func respondToSwipeGesture(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case UISwipeGestureRecognizerDirection.Left:
                self.Upcoming_Btn_Clicked(Btn_Upcoming)
            case UISwipeGestureRecognizerDirection.Right:
                self.Live_Btn_Clicked(Btn_Live)
            default:
                break
            }
        }
    }
    
    @IBAction func Back_Btn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func Live_Btn_Clicked(sender: AnyObject)
    {
        LiveView.hidden=false

        UIView.animateWithDuration(0.4, animations:
            {
                self.LiveView.frame = self.setFrameX(self.LiveView.frame, x: 0)
                self.UpcomingView.frame = self.setFrameX(self.UpcomingView.frame, x: self.UpcomingView.frame.size.width)
                self.BGImg_LiveBtn.frame = self.setFrameX(self.BGImg_LiveBtn.frame , x: self.Btn_Live.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.UpcomingView.hidden=true
        })
        
        Btn_Live.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_Live.userInteractionEnabled=false
        Btn_Live.tag=1
        Btn_Upcoming.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_Upcoming.userInteractionEnabled=true
        Btn_Upcoming.tag=0
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("livematches/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                LiveCategory=[]
                for i in 0  ..< result.count
                {
                    var arr=result[i].dictionaryValue
                    LiveCategory.insert(arr["category_name"]!.string! , atIndex: LiveCategory.count)
                    
                    let tournament = arr["tournaments"]!
                    NumLiveMatch.insert(tournament.count, atIndex: NumLiveMatch.count)
                    
                    var matchId:Array<Int>=[]

                    for j in 0  ..< tournament.count
                    {
                        var tour:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()

                        var to=tournament[j].dictionaryValue
                        
                        tour["category_name"] = arr["category_name"]!.string!
                        tour["id"] = Int(to["id"]!.string!)
                        tour["tournament_type"] = to["tournament_type"]!.string!
                        tour["tournament_name"] = to["tournament_name"]!.string!
                        tour["tournament_image"] = to["image"]!.string!
                        
                        tour["bet_type"] = to["category_type"]!.string!
                        tour["commission"] = to["commission"]!.string!
                        tour["schedule_on"]=to["schedule_on"]!.string!
                        tour["totalbets"]=to["totalbets"]!.int!
                        
                        var teams = to["artists"]!
                        var artists:Array<AnyObject> = Array<AnyObject>()
                        
                        for art in 0  ..< teams.count
                        {
                            var team=teams[art].dictionaryValue
                            
                            var tem:Array<String> = Array<String>()
                            tem.append(team["artist_id"]!.string!)
                            tem.append(team["artist_name"]!.string!)
                            tem.append(team["image"]!.string!)

                            artists.append(tem)
                        }
                        tour["artists"]=artists

                        LiveTournament[Int(to["id"]!.string!)!] = tour
                        matchId.insert(Int(to["id"]!.string!)!, atIndex: matchId.count)
                    }
                    LiveMatchId.insert(matchId, atIndex: i)
                }
            }
        }
        LiveTable.reloadData()
    }
    
    @IBAction func Upcoming_Btn_Clicked(sender: AnyObject)
    {
        UpcomingView.hidden=false
        self.UpcomingView.frame = self.setFrameX(self.UpcomingView.frame, x: self.UpcomingView.frame.size.width)
        self.LiveView.frame = self.setFrameX(self.LiveView.frame, x: 0)
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.LiveView.frame = self.setFrameX(self.LiveView.frame, x: -(self.LiveView.frame.size.width))
                self.UpcomingView.frame = self.setFrameX(self.UpcomingView.frame, x: 0)
                self.BGImg_LiveBtn.frame = self.setFrameX(self.BGImg_LiveBtn.frame , x: self.Btn_Upcoming.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.LiveView.hidden=true
        })

        Btn_Upcoming.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_Upcoming.userInteractionEnabled=false
        Btn_Upcoming.tag=1
        Btn_Live.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_Live.userInteractionEnabled=true
        Btn_Live.tag=0
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("upcomingmatches/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                UpcomeCategory=[]
                for i in 0  ..< result.count
                {
                    var arr=result[i].dictionaryValue
                    UpcomeCategory.insert(arr["category_name"]!.string! , atIndex: UpcomeCategory.count)
                    
                    let tournament = arr["tournaments"]!
                    NumUpcomeMatch.insert(tournament.count, atIndex: NumUpcomeMatch.count)
                    
                    var matchId:Array<Int>=[]
                    
                    for j in 0  ..< tournament.count
                    {
                        var to=tournament[j].dictionaryValue

                        var tour:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
                        tour["category_name"] = arr["category_name"]!.string!
                        tour["id"] = Int(to["id"]!.string!)
                        tour["tournament_type"] = to["tournament_type"]!.string!
                        tour["tournament_name"] = to["tournament_name"]!.string!
                        tour["tournament_image"] = to["image"]!.string!
                        
                        tour["bet_type"] = to["category_type"]!.string!
                        tour["commission"] = to["commission"]!.string!
                        tour["schedule_on"]=to["schedule_on"]!.string!
                        tour["totalbets"]=to["totalbets"]!.int!
                        tour["timeleft"]=to["timeleft"]!.string!

                        var teams = to["artists"]!
                        var artists:Array<AnyObject> = Array<AnyObject>()

                        for art in 0  ..< teams.count
                        {
                            var team=teams[art].dictionaryValue
                            
                            var tem:Array<String> = Array<String>()
                            tem.append(team["artist_id"]!.string!)
                            tem.append(team["artist_name"]!.string!)
                            tem.append(team["image"]!.string!)
                            
                            artists.append(tem)
                        }
                        tour["artists"]=artists
                        
                        UpcomeTournament[Int(to["id"]!.string!)!] = tour
                        matchId.insert(Int(to["id"]!.string!)!, atIndex: matchId.count)
                    }
                    UpcomeMatchId.insert(matchId, atIndex: i)
                }
            }
        }
        UpcomingTable.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView.isEqual(LiveTable)
        {
            return LiveCategory.count
        }
        else if tableView.isEqual(UpcomingTable)
        {
            return UpcomeCategory.count
        }
        return LiveCategory.count
     }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.isEqual(LiveTable)
        {
            if selectLiveCategory == section
            {
                return NumLiveMatch[section]+1
            }
            return 1
        }
        else if tableView.isEqual(UpcomingTable)
        {
            if selectUpcomeCategory == section
            {
                return NumUpcomeMatch[section]+1
            }
            return 1
        }
        
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if tableView.isEqual(LiveTable)
        {
            if indexPath.row == 0
            {
                return 38
            }
            UIView.animateWithDuration(0.4, animations:
                {
                    
                },
                completion:
                {(value: Bool) in
            })
            return 48
        }
        else if tableView.isEqual(UpcomingTable)
        {
            if indexPath.row == 0
            {
                return 38
            }
            UIView.animateWithDuration(0.4, animations:
                {
                    
                },
                completion:
                {(value: Bool) in
            })
            return 48
        }
        
        return 38
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView.isEqual(LiveTable)
        {
            if indexPath.row == 0
            {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("OpenCell", forIndexPath: indexPath) 
                
                let headerView=UIView()
                headerView.backgroundColor=UIColor(patternImage: UIImage(named:"catbg_i5.png")!)
                cell.backgroundView=headerView
                cell.backgroundColor=UIColor.clearColor()
                
                let fnt=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 18))
                cell.textLabel?.font=fnt
                cell.textLabel?.text=LiveCategory[indexPath.section]
                cell.textLabel?.textColor=UIColor.whiteColor()
                cell.textLabel?.frame=CGRectMake(15, 10, 150, 18)
                
                let selectView=UIView()
                selectView.backgroundColor=UIColor.clearColor()
                cell.selectedBackgroundView=selectView
                return cell
            }
            
            let identifier = "OpenBetCell"
            let cell: OpenBetCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? OpenBetCell
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
            cell.backgroundView=headerView
            
            var idArr:Array<Int> = LiveMatchId[indexPath.section]
            let id:Int=idArr[indexPath.row - 1]
            
            for _ in 0  ..< LiveTournament.count
            {
                if let LiveData:Dictionary<String,AnyObject> = LiveTournament[id]! as? Dictionary<String, AnyObject>
                {
                    if LiveData["tournament_type"] as! String == "Tournament"
                    {
                        cell.Team1.setImageWithUrl(NSURL(string:LiveData["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
                        cell.Team1.layer.cornerRadius=19
                        cell.Team1.layer.masksToBounds = true
                        
                        cell.Team2.hidden=true
                        cell.Team2Frm.hidden=true
                        cell.Vs.hidden=true
                    }
                    else if LiveData["tournament_type"] as! String == "Match"
                    {
                        var art1:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
                        var art2:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
                        
                        cell.Team1.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
                        cell.Team2.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
                        
                        cell.Team1.layer.cornerRadius=19
                        cell.Team1.layer.masksToBounds = true
                        
                        cell.Team2.layer.cornerRadius=19
                        cell.Team2.layer.masksToBounds = true
                    }
                    
                    cell.NumUser.text=String(LiveData["totalbets"] as! Int)
                }
            }
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.blackColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()
            
            return cell!
        }
        else if tableView.isEqual(UpcomingTable)
        {
            if indexPath.row == 0
            {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("OpenCell", forIndexPath: indexPath) 
                
                let headerView=UIView()
                headerView.backgroundColor=UIColor(patternImage: UIImage(named:"catbg_i5.png")!)
                cell.backgroundView=headerView
                cell.backgroundColor=UIColor.clearColor()
                
                let fnt=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 18))
                cell.textLabel?.font=fnt
                cell.textLabel?.text=UpcomeCategory[indexPath.section]
                cell.textLabel?.textColor=UIColor.whiteColor()
                cell.textLabel?.frame=CGRectMake(15, 10, 150, 18)
                
                let selectView=UIView()
                selectView.backgroundColor=UIColor.clearColor()
                cell.selectedBackgroundView=selectView
                return cell
            }
            
            let identifier = "OpenBetCell"
            let cell: OpenBetCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? OpenBetCell
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
            cell.backgroundView=headerView
            
            var idArr:Array<Int> = UpcomeMatchId[indexPath.section]
            let id:Int=idArr[indexPath.row - 1]
            
            for _ in 0  ..< UpcomeTournament.count 
            {
                if let LiveData:Dictionary<String,AnyObject> = UpcomeTournament[id]! as? Dictionary<String, AnyObject>
                {
                    if LiveData["tournament_type"] as! String == "Tournament"
                    {
                        cell.Team1.setImageWithUrl(NSURL(string:LiveData["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
                        cell.Team1.layer.cornerRadius=19
                        cell.Team1.layer.masksToBounds = true
                        
                        cell.Team2.hidden=true
                        cell.Team2Frm.hidden=true
                        cell.Vs.hidden=true
                    }
                    else if LiveData["tournament_type"] as! String == "Match"
                    {
                        var art1:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
                        var art2:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
                        
                        cell.Team1.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
                        cell.Team2.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
                        
                        cell.Team1.layer.cornerRadius=19
                        cell.Team1.layer.masksToBounds = true
                        
                        cell.Team2.layer.cornerRadius=19
                        cell.Team2.layer.masksToBounds = true
                    }
                    cell.TimeLeft.hidden=false
                    cell.TimeLeft.text=LiveData["timeleft"] as? String
                    
                    cell.NumUser.text=String(LiveData["totalbets"] as! Int)
                }
            }
        
            let selectView=UIView()
            selectView.backgroundColor=UIColor.blackColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()
            
            return cell!
        }
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        if tableView.isEqual(LiveTable)
        {
            if indexPath.row == 0
            {
                if selectLiveCategory == indexPath.section
                {
                    selectLiveCategory = -1
                }
                else
                {
                    selectLiveCategory=indexPath.section
                }
                let transition = CATransition()
                transition.type = kCATransitionFade
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
                transition.fillMode = kCAFillModeForwards
                transition.duration = 0.8
                transition.subtype = kCATransitionFromTop
                self.LiveTable.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
                LiveTable.reloadData()
            }
            else
            {
                SelectSection = indexPath.section
                SelectRow = indexPath.row
                self.performSegueWithIdentifier("LiveToSelectUser", sender: self)
            }
        }
        else if tableView.isEqual(UpcomingTable)
        {
            if indexPath.row == 0
            {
                if selectUpcomeCategory == indexPath.section
                {
                    selectUpcomeCategory = -1
                }
                else
                {
                    selectUpcomeCategory=indexPath.section
                }
                let transition = CATransition()
                transition.type = kCATransitionFade
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
                transition.fillMode = kCAFillModeForwards
                transition.duration = 0.8
                transition.subtype = kCATransitionFromTop
                self.UpcomingTable.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
                UpcomingTable.reloadData()
            }
            else
            {
                SelectSection = indexPath.section
                SelectRow = indexPath.row
                self.performSegueWithIdentifier("LiveToSelectUser", sender: self)
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "LiveToSelectUser"
        {
            let secondViewController = (segue.destinationViewController as! SelectUser)
            NSUserDefaults.standardUserDefaults().setObject("SelectMatch" , forKey: "BettingView")

            if Btn_Live.tag == 1
            {
                var idArr:Array<Int> = LiveMatchId[SelectSection]
                let id:Int=idArr[SelectRow - 1]
                
                for _ in 0  ..< LiveTournament.count 
                {
                    if let LiveData:Dictionary<String,AnyObject> = LiveTournament[id]! as? Dictionary<String, AnyObject>
                    {
                        secondViewController.LiveData=LiveData
                    }
                }
            }
            else if Btn_Upcoming.tag == 1
            {
                var idArr:Array<Int> = UpcomeMatchId[SelectSection]
                let id:Int=idArr[SelectRow - 1]
                
                for _ in 0  ..< UpcomeTournament.count
                {
                    if let LiveData:Dictionary<String,AnyObject> = UpcomeTournament[id]! as? Dictionary<String, AnyObject>
                    {
                        secondViewController.LiveData=LiveData
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
