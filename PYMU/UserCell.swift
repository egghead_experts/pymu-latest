//
//  UserCell.swift
//  PYMU
//
//  Created by Apple on 24/07/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell
{
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgFrm: UIImageView!
    
    @IBOutlet var Status: UIButton!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDoler: UILabel!
    
    @IBOutlet var Accept: UIButton!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
