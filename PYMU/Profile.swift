//
//  Profile.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class Profile: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate, TOCropViewControllerDelegate
{
    var ApiObj = PymuAPI()

    @IBOutlet var Btn_Menu_Profile: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    @IBOutlet var Btn_Back_DoneView: UIButton!
    @IBOutlet var Btn_Edit: UIButton!
    @IBOutlet var Btn_Done: UIButton!
    
    @IBOutlet var UserProfile: UIImageView!
    @IBOutlet var ProfileFrm: UIImageView!
    @IBOutlet var PicEdit: UIButton!
    @IBOutlet var NameTxt: UITextField!
    @IBOutlet var FirstNameTxt: UITextField!
    @IBOutlet var LastNameTxt: UITextField!
    
    @IBOutlet var EmailTxt: UITextField!
    @IBOutlet var CardNumTxt: UITextField!
    
    @IBOutlet var CardView: UIView!
    @IBOutlet var DateTxt: UITextField!
    @IBOutlet var CvcTxt: UITextField!
    @IBOutlet var ZipTxt: UITextField!
    
    @IBOutlet var OldPassTxt: UITextField!
    @IBOutlet var NewPassTxt: UITextField!
    @IBOutlet var ConformPassTxt: UITextField!
    
    @IBOutlet var RawBg_Height: [NSLayoutConstraint]!
    @IBOutlet var CatBg_Height: [NSLayoutConstraint]!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)

        Btn_Menu_Profile.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            for h in RawBg_Height
            {
                h.constant=32
            }
            
            for hh in CatBg_Height
            {
                hh.constant=22
            }
        }
        
        FirstNameTxt.delegate=self
        LastNameTxt.delegate=self
        OldPassTxt.delegate=self
        NewPassTxt.delegate=self
        ConformPassTxt.delegate=self
        CardNumTxt.delegate=self
        DateTxt.delegate=self
        CvcTxt.delegate=self
        ZipTxt.delegate=self
        
        self.ProfileView_Edit_Done(false)
        EmailTxt.enabled=false
        NameTxt.enabled=false
        UserProfile.layer.cornerRadius=27
        UserProfile.layer.masksToBounds = true
        ProfileFrm.image=UIImage(named:"team_frm_in.png")
        self.view.bringSubviewToFront(PicEdit)
        self.ProfileDataLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(Profile.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    func ProfileDataLoad()
    {
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/viewprofile/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["first_name"].string
            {
                //UserProfile.setImageWithUrl(NSURL(string:dataDict["profilepic"].string!)!, placeHolderImage: UIImage(named:"team_bg"))
                if let url = NSURL(string: dataDict["profilepic"].string!)
                {
                    if let data = NSData(contentsOfURL: url)
                    {
                        UserProfile.image = UIImage(data: data)
                    }        
                }
                
                FirstNameTxt.text=dataDict["first_name"].string!
                LastNameTxt.text=dataDict["last_name"].string!
                NameTxt.text="\(FirstNameTxt.text!) \(LastNameTxt.text!)"
                
                EmailTxt.text=dataDict["email"].string!
                CardNumTxt.text=dataDict["card_number"].string!
                DateTxt.text=dataDict["expire_on"].string!
                CvcTxt.text=dataDict["cvv_number"].string!
                ZipTxt.text=dataDict["billing_zip"].string!
                
                NSUserDefaults.standardUserDefaults().setObject("\(dataDict["first_name"].string!) \(dataDict["last_name"].string!)" , forKey: "UserName")
                NSUserDefaults.standardUserDefaults().setObject(dataDict["profilepic"].string! , forKey: "UserPic")
            }
        }
    }
    
    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height : UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height : UIScreen.mainScreen().bounds.size.width

        scrollView.contentInset = UIEdgeInsetsZero
        scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
        scrollView.contentOffset = CGPointMake(0.0, 0.0)
    }
    
    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        self.setAllTextFieldKeyBoardHidden()
        Btn_BackMenu.hidden=false
    }
    
    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        self.setAllTextFieldKeyBoardHidden()
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    
    @IBAction func Pic_Edit_Clicked(sender: AnyObject)
    {
        let optionMenu = UIAlertController(title: nil, message: "Select photo from", preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        })
        let saveAction = UIAlertAction(title: "Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .Camera
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        if let popoverController = optionMenu.popoverPresentationController
        {
            popoverController.sourceView = sender as? UIView
            popoverController.sourceRect = sender.bounds
        }
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            if image != nil
            {
                let cropController:TOCropViewController = TOCropViewController(image: image)
                cropController.delegate=self
                cropController.aspectRatioLocked=true
                cropController.defaultAspectRatio=TOCropViewControllerAspectRatio.RatioSquare
                
                self.presentViewController(cropController, animated: true, completion: nil)
            }
            self.rotated()
        })
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker.dismissViewControllerAnimated(false, completion: { () -> Void in
            self.rotated()
        })
    }
    
    func cropViewController(cropViewController: TOCropViewController!, didCropToImage image: UIImage!, withRect cropRect: CGRect, angle: Int)
    {
        cropViewController.dismissViewControllerAnimated(true) { () -> Void in
            self.UserProfile.image = image
        }
    }
    
    func cropViewController(cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool)
    {
        cropViewController.dismissViewControllerAnimated(true) { () -> Void in  }
    }
    
    @IBAction func Edit_Btn_Clicked(sender: AnyObject)
    {
        self.ProfileView_Edit_Done(true)
        ProfileFrm.image=UIImage(named:"usr_cng_img.png")
    }
    
    @IBAction func Done_Btn_Clicked(sender: AnyObject)
    {
        var msg:String = ""
        let newPass=NewPassTxt.text
        let conformPass=ConformPassTxt.text
        
        if FirstNameTxt.text!.isEmpty
        {
            msg="Please enter first name"
        }
        else if LastNameTxt.text!.isEmpty
        {
            msg="Please enter last name"
        }
        else if CardNumTxt.text!.isEmpty
        {
            msg="Please enter card number"
        }
        else if CardNumTxt.text?.characters.count < 19
        {
            msg="Please enter proper card number"
        }
        else if DateTxt.text!.isEmpty
        {
            msg="Please enter expire date"
        }
        else if DateTxt.text?.characters.count < 5
        {
            msg="Please enter proper expire date"
        }
        else if CvcTxt.text!.isEmpty
        {
            msg="Please enter cvc number"
        }
        else if CvcTxt.text?.characters.count < 3
        {
            msg="Please enter proper cvc number"
        }
        else if ZipTxt.text!.isEmpty
        {
            msg="Please enter zip code"
        }
        else if ZipTxt.text?.characters.count < 5
        {
            msg="Please enter proper zip code"
        }
        else if OldPassTxt.text!.isEmpty && NewPassTxt.text!.isEmpty && ConformPassTxt.text!.isEmpty
        {
            let param = ["first_name"  : FirstNameTxt.text!, "last_name":LastNameTxt.text!, "card_number":CardNumTxt.text!, "expire_on":DateTxt.text!, "cvv_number":CvcTxt.text!, "billing_zip":ZipTxt.text!]
            
            let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
            var dataDict=ApiObj.CallUpdatePicture("user/editprofile/\(urlStr)", parameters: param, pic: self.UserProfile)
            
            if (dataDict.null == nil)
            {
                if dataDict == 6
                {
                    TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
                }
                else if let _ = dataDict["msg"].string
                {
                    NameTxt.text="\(FirstNameTxt.text!) \(LastNameTxt.text!)"
                    NSUserDefaults.standardUserDefaults().setObject(NameTxt.text! , forKey: "UserName")
                    //NSUserDefaults.standardUserDefaults().setObject(dataDict["profilepic"].string , forKey: "UserPic")
                    
                    self.ProfileView_Edit_Done(false)
                    ProfileFrm.image=UIImage(named:"team_frm_in.png")
                    self.setAllTextFieldKeyBoardHidden()
                }
            }
        }
        else if OldPassTxt.text!.isEmpty
        {
            msg="Please enter old password"
        }
        else if NewPassTxt.text!.isEmpty
        {
            msg="Please enter new password"
        }
        else if ConformPassTxt.text!.isEmpty
        {
            msg="Please enter conform password"
        }
        else if newPass != conformPass
        {
            msg="Conform password not match"
        }
        else
        {
            let param = ["first_name"  : FirstNameTxt.text!, "last_name":LastNameTxt.text!, "card_number":CardNumTxt.text!, "expire_on":DateTxt.text!, "cvv_number":CvcTxt.text!, "billing_zip":ZipTxt.text!, "oldpwd":OldPassTxt.text!, "newpwd":NewPassTxt.text!]
            
            let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
            var dataDict=ApiObj.CallUpdatePicture("user/editprofile/\(urlStr)", parameters: param, pic: self.UserProfile)
            
            if (dataDict.null == nil)
            {
                if dataDict == 6
                {
                    TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
                }
                else if let _ = dataDict["msg"].string
                {
                    NameTxt.text="\(FirstNameTxt.text!) \(LastNameTxt.text!)"
                    NSUserDefaults.standardUserDefaults().setObject(NameTxt.text! , forKey: "UserName")

                    OldPassTxt.text=""
                    NewPassTxt.text=""
                    ConformPassTxt.text=""
                    
                    self.ProfileView_Edit_Done(false)
                    ProfileFrm.image=UIImage(named:"team_frm_in.png")
                    self.setAllTextFieldKeyBoardHidden()
                }
                else
                {
                    msg = "Invalid old password"
                }
            }
        }
        
        if msg != ""
        {
            let alertController = UIAlertController(title: "PYMU", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func DoneView_Back_Clicked(sender: AnyObject)
    {
        self.ProfileDataLoad()
        self.ProfileView_Edit_Done(false)
        ProfileFrm.image=UIImage(named:"team_frm_in.png")
        self.setAllTextFieldKeyBoardHidden()
    }
    
    
    func ProfileView_Edit_Done(bol:Bool)
    {
        CardView.hidden = !bol
        
        Btn_Menu_Profile.hidden = bol
        Btn_Back_DoneView.hidden = !bol
        
        Btn_Edit.hidden = bol
        Btn_Done.hidden = !bol
        
        PicEdit.userInteractionEnabled = bol
        
        NameTxt.hidden = bol
        FirstNameTxt.hidden = !bol
        LastNameTxt.hidden = !bol
        
        FirstNameTxt.enabled = bol
        LastNameTxt.enabled = bol
        OldPassTxt.enabled = bol
        NewPassTxt.enabled = bol
        ConformPassTxt.enabled = bol
        CardNumTxt.enabled = bol
        DateTxt.enabled = bol
        CvcTxt.enabled = bol
        ZipTxt.enabled = bol
    }
    
    func setAllTextFieldKeyBoardHidden()
    {
        FirstNameTxt.resignFirstResponder()
        LastNameTxt.resignFirstResponder()
        OldPassTxt.resignFirstResponder()
        NewPassTxt.resignFirstResponder()
        ConformPassTxt.resignFirstResponder()
        CardNumTxt.resignFirstResponder()
        DateTxt.resignFirstResponder()
        CvcTxt.resignFirstResponder()
        ZipTxt.resignFirstResponder()
    }
    
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if textField == CardNumTxt
        {
            let replacementStringIsLegal = string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "0123456789").invertedSet) == nil
            
            if !replacementStringIsLegal
            {
                return false
            }
            
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: "0123456789").invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 16 && !hasLeadingOne) || length > 19
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 16) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if length - index > 4
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 4))
                formattedString.appendFormat("%@-", prefix)
                index += 4
            }

            if length - index > 4
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 4))
                formattedString.appendFormat("%@-", prefix)
                index += 4
            }
            if length - index > 4
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 4))
                formattedString.appendFormat("%@-", prefix)
                index += 4
            }

            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            return false
        }
        else if textField == DateTxt
        {
            let replacementStringIsLegal = string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "0123456789").invertedSet) == nil
            
            if !replacementStringIsLegal
            {
                return false
            }
            
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: "0123456789").invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 4 && !hasLeadingOne) || length > 5
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 4) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if length - index > 2
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 2))
                formattedString.appendFormat("%@/", prefix)
                index += 2
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            
            return false
        }
        else if textField == CvcTxt
        {
            var result = true
            let prospectiveText = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            if string.characters.count > 0
            {
                let disallowedCharacterSet = NSCharacterSet(charactersInString: "0123456789").invertedSet
                let replacementStringIsLegal = string.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
                
                let resultingStringLengthIsLegal = prospectiveText.characters.count <= 3
                
                let scanner = NSScanner(string: prospectiveText)
                let resultingTextIsNumeric = scanner.scanDecimal(nil) && scanner.atEnd
                
                result = replacementStringIsLegal && resultingStringLengthIsLegal && resultingTextIsNumeric
            }
            return result
        }
        else if textField == ZipTxt
        {
            var result = true
            let prospectiveText = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            if string.characters.count > 0
            {
                let disallowedCharacterSet = NSCharacterSet(charactersInString: "0123456789").invertedSet
                let replacementStringIsLegal = string.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
                
                let resultingStringLengthIsLegal = prospectiveText.characters.count <= 5
                
                let scanner = NSScanner(string: prospectiveText)
                let resultingTextIsNumeric = scanner.scanDecimal(nil) && scanner.atEnd
                
                result = replacementStringIsLegal && resultingStringLengthIsLegal && resultingTextIsNumeric
            }
            return result
        }
        else
        {
            return true
        }
    }

    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if UIScreen.mainScreen().bounds.size.height == 736.0
        {
            if textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -60
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 667.0
        {
            if textField.isEqual(ZipTxt) || textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -120
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 568.0
        {
            if textField.isEqual(CvcTxt) || textField.isEqual(ZipTxt) || textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -216
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            if textField.isEqual(CardNumTxt) || textField.isEqual(DateTxt) || textField.isEqual(CvcTxt) || textField.isEqual(ZipTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -140
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
            if textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -215
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if UIScreen.mainScreen().bounds.size.height == 736.0
        {
            if textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 667.0
        {
            if textField.isEqual(ZipTxt) || textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 568.0
        {
            if textField.isEqual(CvcTxt) || textField.isEqual(ZipTxt) || textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 480
        {
            if textField.isEqual(CardNumTxt) || textField.isEqual(DateTxt) || textField.isEqual(CvcTxt) || textField.isEqual(ZipTxt) || textField.isEqual(OldPassTxt) || textField.isEqual(NewPassTxt) || textField.isEqual(ConformPassTxt)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    func touchOutside(touchGesture:UITapGestureRecognizer) -> Void
    {
        self.setAllTextFieldKeyBoardHidden()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField.isEqual(FirstNameTxt)
        {
            LastNameTxt.becomeFirstResponder()
        }
        else if textField.isEqual(LastNameTxt)
        {
            textField.resignFirstResponder()
        }
        else if textField.isEqual(CardNumTxt)
        {
            DateTxt.becomeFirstResponder()
        }
        else if textField.isEqual(DateTxt)
        {
            CvcTxt.becomeFirstResponder()
        }
        else if textField.isEqual(CvcTxt)
        {
            ZipTxt.becomeFirstResponder()
        }
        else if textField.isEqual(ZipTxt)
        {
            textField.resignFirstResponder()
        }
        else if textField.isEqual(OldPassTxt)
        {
            NewPassTxt.becomeFirstResponder()
        }
        else if textField.isEqual(NewPassTxt)
        {
            ConformPassTxt.becomeFirstResponder()
        }
        else if textField.isEqual(ConformPassTxt)
        {
            textField.resignFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.setAllTextFieldKeyBoardHidden()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {}
}
