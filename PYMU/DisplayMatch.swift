//
//  DisplayMatch.swift
//  PYMU
//
//  Created by Egghead IOS3 on 10/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit
import coinbase_official

class DisplayMatch: UIViewController, UIGestureRecognizerDelegate , UIAlertViewDelegate, UIScrollViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var GamePlace: UILabel!
    
    @IBOutlet var Team1_Img: UIImageView!
    @IBOutlet var Team1_Frm: UIImageView!
    @IBOutlet var Team2_Img: UIImageView!
    @IBOutlet var Team2_Frm: UIImageView!
    @IBOutlet var Team1_Name: UILabel!
    @IBOutlet var Team2_Name: UILabel!
    
    @IBOutlet var BetView: UIView!
    
    @IBOutlet var Btn_Spread: UIButton!
    @IBOutlet var SpreadPoints: UILabel!
    @IBOutlet var SpreadBet: UILabel!
    
    @IBOutlet var Btn_Lose: UIButton!
    @IBOutlet var WON_Btn: UIButton!
    @IBOutlet var LOSE_Btn: UIButton!
    @IBOutlet var LoseBet: UILabel!
    
    @IBOutlet var Btn_Over: UIButton!
    @IBOutlet var OVER_Btn: UIButton!
    @IBOutlet var UNDER_Btn: UIButton!
    @IBOutlet var OverPoints: UILabel!
    @IBOutlet var OverBet: UILabel!
    
    @IBOutlet var Btn_PYMU: UIButton!
    
    @IBOutlet var PYMUBtn_BottomSpace: NSLayoutConstraint!
    @IBOutlet var WinLoseBtn_TopSpace: NSLayoutConstraint!
    @IBOutlet var SpreadBtn_TopSpace: NSLayoutConstraint!
    @IBOutlet var OverBtn_TopSpace: NSLayoutConstraint!
    
    @IBOutlet var PopupView: UIView!
    @IBOutlet var OpenBetAlert: UIView!
    @IBOutlet var OpenBetMessage: UILabel!
    @IBOutlet var InviteAlert: UIView!
    @IBOutlet var InviteMessage: UILabel!
    @IBOutlet var BetAmmountLbl: UILabel!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    
    var LiveData:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
    
    var Bet_Id:Int = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setViewData()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DisplayMatch.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
    }
    
    
    func setViewData()
    {
        GamePlace.text=LiveData["tournament_name"]! as? String
        
        var art1:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
        var art2:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
        
        Team1_Img.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
        Team2_Img.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
        Team1_Name.text=art1[1]
        Team2_Name.text=art2[1]
        
        Team1_Img.layer.cornerRadius=27
        Team1_Img.layer.masksToBounds = true
        Team2_Img.layer.cornerRadius=27
        Team2_Img.layer.masksToBounds = true
        
        if art1[0] == LiveData["artist_id"]! as? String
        {
            Team1_Frm.image=UIImage(named: "team_frm_in_sel.png")
            Team2_Frm.image=UIImage(named: "team_frm_in.png")
        }
        else if art2[0] == LiveData["artist_id"]! as? String
        {
            Team1_Frm.image=UIImage(named: "team_frm_in.png")
            Team2_Frm.image=UIImage(named: "team_frm_in_sel.png")
        }
        
        self.AllBtnShow()
        
        if LiveData["bet_for"]! as? String == "spread"
        {
            Btn_Spread.setImage(UIImage(named: "bat_spred_bg.png"), forState: UIControlState.Normal)
            BetView.sendSubviewToBack(Btn_Spread)
            
            SpreadPoints.text=LiveData["points"]! as? String
            SpreadBet.text=LiveData["bet_amount"]! as? String
        }
        else if (LiveData["bet_for"]! as? String == "win") || (LiveData["bet_for"]! as? String == "lose")
        {
            Btn_Lose.setImage(UIImage(named: "bat_lose_bg.png"), forState: UIControlState.Normal)
            BetView.sendSubviewToBack(Btn_Lose)
            
            if (LiveData["bet_for"]! as? String == "win")
            {
                WON_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
                LOSE_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            }
            else
            {
                WON_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
                LOSE_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
            }

            LoseBet.text=LiveData["bet_amount"]! as? String
        }
        else if (LiveData["bet_for"]! as? String == "over") || (LiveData["bet_for"]! as? String == "under")
        {
            Btn_Over.setImage(UIImage(named: "bat_over_bg.png"), forState: UIControlState.Normal)
            BetView.sendSubviewToBack(Btn_Over)
            
            if (LiveData["bet_for"]! as? String == "over")
            {
                OVER_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
                UNDER_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            }
            else
            {
                UNDER_Btn.setTitleColor(UIColor(red: 169.0/255.0, green: 255.0/255.0, blue: 11.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
                OVER_Btn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
            }
            
            OverPoints.text=LiveData["points"]! as? String
            OverBet.text=LiveData["bet_amount"]! as? String
        }
        
        PopupView.hidden=true
        contentView.bringSubviewToFront(PopupView)
        
        if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            SpreadBtn_TopSpace.constant=5
            WinLoseBtn_TopSpace.constant=5
            OverBtn_TopSpace.constant=5
            
            PYMUBtn_BottomSpace.constant=5
        }
        else if UIScreen.mainScreen().bounds.size.height == 320.0 && UIScreen.mainScreen().bounds.size.width == 480.0
        {
            SpreadBtn_TopSpace.constant=5
            WinLoseBtn_TopSpace.constant=5
            OverBtn_TopSpace.constant=5
            
            PYMUBtn_BottomSpace.constant=5
        }
        else
        {
            SpreadBtn_TopSpace.constant=25
            WinLoseBtn_TopSpace.constant=15
            OverBtn_TopSpace.constant=15
            
            PYMUBtn_BottomSpace.constant=15
        }
    }
    
    
    @IBAction func BackBtn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
        /*let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)*/
    }
    
   
    // --------------------
    //   *** Bet View ***
    // --------------------
    
    func AllBtnShow()
    {
        Btn_Spread.setImage(UIImage(named: "btn_spread.png"), forState: UIControlState.Normal)
        Btn_Lose.setImage(UIImage(named: "btn_lose.png"), forState: UIControlState.Normal)
        Btn_Over.setImage(UIImage(named: "btn_over.png"), forState: UIControlState.Normal)
        
        BetView.bringSubviewToFront(Btn_Spread)
        BetView.bringSubviewToFront(Btn_Lose)
        BetView.bringSubviewToFront(Btn_Over)
    }
    
    @IBAction func PYMU_Btn_Clicked(sender: AnyObject)
    {
        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
        {
            UIView.animateWithDuration(0.2, animations:
                {
                    self.PopupView.hidden=false
                    self.InviteAlert.layer.masksToBounds=true
                    self.InviteMessage.text="Are you sure want to bet with \(self.LiveData["user_name"]!)"
                    self.BetAmmountLbl.text="$\(self.LiveData["bet_amount"]!)"
                    self.InviteAlert.hidden=false
                    self.OpenBetAlert.hidden=true
                    self.PopupView.alpha=0.7
                    self.PopupView.alpha=1.0
                },
                completion:
                {(value: Bool) in
            })
        }
        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
        {
            UIView.animateWithDuration(0.2, animations:
                {
                    self.PopupView.hidden=false
                    self.OpenBetAlert.layer.masksToBounds=true
                    self.OpenBetMessage.text="Are you sure want to bet with \(self.LiveData["user_name"]!)?"
                    self.OpenBetAlert.hidden=false
                    self.InviteAlert.hidden=true
                    self.PopupView.alpha=0.7
                    self.PopupView.alpha=1.0
                },
                completion:
                {(value: Bool) in
            })
        }
    }
    
    @IBAction func Invite_Accept_Clicked(sender: AnyObject)
    {
        CoinbaseOAuth.startOAuthAuthenticationWithClientId(CLIENT_ID , scope: "user balance", redirectUri: "com.egghead.pymu.coinbase-oauth://coinbase-oauth", meta: nil)
        NSUserDefaults.standardUserDefaults().setObject("MatchBetInvite" , forKey: "coinbase")
    }
    
    func authenticationComplete(response:[String : AnyObject])
    {
        Bet_Id=Int(LiveData["bet_id"] as! String)!
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "bet_id=\(Bet_Id)&response=accept"
        
        var dataDict=ApiObj.CallPostApi("user/acceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    },
                                           completion:
                    {(value: Bool) in
                })
            }
        }        
    }
    
    @IBAction func Invite_Decline_Clicked(sender: AnyObject)
    {
        Bet_Id=Int(LiveData["bet_id"] as! String)!
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "bet_id=\(Bet_Id)&response=declined"
        
        var dataDict=ApiObj.CallPostApi("user/acceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    @IBAction func Open_Bet_Accept_Clicked(sender: AnyObject)
    {
        CoinbaseOAuth.startOAuthAuthenticationWithClientId(CLIENT_ID , scope: "user balance", redirectUri: "com.egghead.pymu.coinbase-oauth://coinbase-oauth", meta: nil)

        NSUserDefaults.standardUserDefaults().setObject("MatchBetOpen" , forKey: "coinbase")

    }
    
    func authenticationComplete1(response:[String : AnyObject])
    {
        Bet_Id=Int(LiveData["bet_id"] as! String)!
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "bet_id=\(Bet_Id)&response=accept"
        
        var dataDict=ApiObj.CallPostApi("user/acceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopupView.hidden=true
                        if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "Invitation"
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else if (NSUserDefaults.standardUserDefaults().objectForKey("DisplayBet") as! String) == "OpenBet"
                        {
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                        }
                    },
                                           completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    @IBAction func Open_Bet_Cancel_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopupView.hidden=true
            },
            completion:
            {(value: Bool) in
        })
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
