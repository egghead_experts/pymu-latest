//
//  TBetListCell.swift
//  PYMU
//
//  Created by Egghead IOS3 on 14/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit

class TBetListCell: UITableViewCell
{
    @IBOutlet var Team1: UIImageView!
    @IBOutlet var Team1Frm: UIImageView!
    
    @IBOutlet var NameUser: UILabel!
    @IBOutlet var BetTime: UILabel!
    @IBOutlet var NumBetor: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
