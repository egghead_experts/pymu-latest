//
//  BetSumCell.swift
//  PYMU
//
//  Created by Apple on 25/07/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class BetSumCell: UITableViewCell
{

    @IBOutlet var TournamentImg: UIImageView!
    @IBOutlet var TournamentFrm: UIImageView!
    @IBOutlet var Team1: UIImageView!
    @IBOutlet var Team1Frm: UIImageView!
    @IBOutlet var Team2: UIImageView!
    @IBOutlet var Team2Frm: UIImageView!
    @IBOutlet var vsImg: UIImageView!
    
    @IBOutlet var Lose_WinImg: UIImageView!
    
    @IBOutlet var NameUser: UILabel!
    @IBOutlet var GameName: UILabel!
    
    @IBOutlet var Account_Name_Width: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
