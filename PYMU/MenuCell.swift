//
//  MenuCell.swift
//  PYMU
//
//  Created by Egghead IOS3 on 21/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell
{

    @IBOutlet var MenuImg: UIImageView!
    @IBOutlet var MenuName: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
