//
//  AppDelegate.swift
//  PYMU
//
//  Created by Apple on 27/08/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit
import coinbase_official

let CLIENT_ID = "d884b1e090f610437afd38fa33a402503a8740358488e9044f6043d3a734bf8e"
let CLIENT_SECRET = "6691a1a1c49cbd5160ff3bb63086bbf83ece24bd617d80d4d1f758df8b487d0d"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var nav:UINavigationController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        window=UIWindow(frame: UIScreen.mainScreen().bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController:UIViewController = storyboard.instantiateViewControllerWithIdentifier("LoginView")
        
        navigationController.viewControllers = [rootViewController]
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        window?.backgroundColor=UIColor.blackColor()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        if url.scheme == "com.egghead.pymu.coinbase-oauth"
        {
            
            CoinbaseOAuth.finishOAuthAuthenticationForUrl(url, clientId: CLIENT_ID, clientSecret: CLIENT_SECRET, completion: { (result : AnyObject?, error: NSError?) -> Void in
                if error != nil
                {
                    // Could not authenticate.
                }
                else
                {
                    if let result = result as? [String : AnyObject]
                    {
                        switch NSUserDefaults.standardUserDefaults().valueForKey("coinbase") as! String
                        {
                        case "MatchBet":
                            let sw = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! SWRevealViewController
                            let sv = ((sw.frontViewController as! UINavigationController).visibleViewController) as! MatchBet
                            sv.authenticationComplete(result)
                            break
                        case "TournamentBet":
                            let sw = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! SWRevealViewController
                            let sv = ((sw.frontViewController as! UINavigationController).visibleViewController) as! TournamentBet
                            sv.authenticationComplete(result)
                            break
                        case "MatchBetInvite":
                            let sv = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! DisplayMatch
                            sv.authenticationComplete(result)
                            break
                        case "MatchBetOpen":
                            let sv = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! DisplayMatch
                            sv.authenticationComplete1(result)
                            break
                        case "TournamentBetInvite":
                            let sv = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! DisplayTournament
                            sv.authenticationComplete(result)
                            break
                        case "TournamentBetOpen":
                            let sv = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! DisplayTournament
                            sv.authenticationComplete1(result)
                            break
                        default:
                            break
                        }
                    }
                    
                    
                    /*
                     
                     let sv = (((self.window?.rootViewController)! as! UINavigationController).visibleViewController)! as! MainView
                     sv.authenticationComplete(result)
                     
                    MatchBet
                    TournamentBet
                    
                    MatchBetInvite
                    MatchBetOpen
                    
                    TournamentBetInvite
                    TournamentBetOpen*/
                    
                }
            })
            return true
        }
        else if url.scheme == "fb1465704213730550"
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        else
        {
            return false
        }
        
        //return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
  
    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

