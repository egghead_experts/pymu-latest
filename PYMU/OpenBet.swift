//
//  OpenBet.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class OpenBet: UIViewController , UITableViewDataSource , UITableViewDelegate 
{
    var ApiObj = PymuAPI()

    @IBOutlet var OpenbetView: UIView!
    @IBOutlet var Btn_Menu: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    @IBOutlet var OpenBetTable: UITableView!
    
    var selectCategory = 0
    var Category:Array<String> = []
    var NumMatch:Array<Int> = []
    var MatchId:Array<Array<Int>> = []
    var AllTournament:Dictionary<Int, AnyObject> = Dictionary<Int, AnyObject>()
    
    var SelectSection:Int = 0
    var SelectRow:Int = 0


    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)

        Btn_Menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.OpenBetViewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func OpenBetViewDidLoad()
    {
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/openbet/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                Category=[]

                for i in 0  ..< result.count
                {
                    var arr=result[i].dictionaryValue
                    Category.insert(arr["category_name"]!.string! , atIndex: Category.count)
                    
                    let tournament = arr["tournaments"]!
                    NumMatch.insert(tournament.count, atIndex: NumMatch.count)
                    
                    var matchId:Array<Int>=[]
                    
                    for j in 0  ..< tournament.count
                    {
                        var to=tournament[j].dictionaryValue
                        
                        var tour:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
                        tour["category_name"] = arr["category_name"]!.string!
                        tour["id"] = to["id"]!.string!
                        tour["tournament_type"] = to["tournament_type"]!.string!
                        tour["tournament_name"] = to["tournament_name"]!.string!
                        tour["tournament_image"] = to["image"]!.string!
                        tour["totalbets"]=to["totalbets"]!.string!
                        
                        var teams = to["artists"]!
                        var artists:Array<AnyObject> = Array<AnyObject>()
                        
                        for art in 0  ..< teams.count
                        {
                            var team=teams[art].dictionaryValue
                            
                            var tem:Array<String> = Array<String>()
                            tem.append(team["artist_id"]!.string!)
                            tem.append(team["artist_name"]!.string!)
                            tem.append(team["image"]!.string!)
                            
                            artists.append(tem)
                        }
                        tour["artists"]=artists
                        
                        AllTournament[Int(to["id"]!.string!)!] = tour
                        matchId.insert(Int(to["id"]!.string!)!, atIndex: matchId.count)
                    }
                    MatchId.insert(matchId, atIndex: i)
                }
            }
        }
        OpenBetTable.reloadData()
    }
    
    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=false
    }

    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    
    // All Table Methods....
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return Category.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectCategory == section
        {
            return NumMatch[section]+1
        }
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 38
        }
        return 48
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("OpenCell", forIndexPath: indexPath)
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"catbg_i5.png")!)
            cell.backgroundView=headerView
            cell.backgroundColor=UIColor.clearColor()
            
            let fnt=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 18))
            cell.textLabel?.font=fnt
            cell.textLabel?.text=Category[indexPath.section]
            cell.textLabel?.textColor=UIColor.whiteColor()
            cell.textLabel?.frame=CGRectMake(15, 10, 150, 18)
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.clearColor()
            cell.selectedBackgroundView=selectView
            return cell
        }
        
        let cell: OpenBetCell! = tableView.dequeueReusableCellWithIdentifier("OpenBetCell") as? OpenBetCell
        
        let headerView=UIView()
        headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
        cell.backgroundView=headerView
        
        var idArr:Array<Int> = MatchId[indexPath.section]
        let id:Int=idArr[indexPath.row - 1]
        
        for _ in 0  ..< AllTournament.count
        {
            if let LiveData:Dictionary<String,AnyObject> = AllTournament[id]! as? Dictionary<String, AnyObject>
            {
                if LiveData["tournament_type"] as! String == "Tournament"
                {
                    cell.Team1.setImageWithUrl(NSURL(string:LiveData["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
                    cell.Team1.layer.cornerRadius=19
                    cell.Team1.layer.masksToBounds = true
                    
                    cell.Team2.hidden=true
                    cell.Team2Frm.hidden=true
                    cell.Vs.hidden=true
                
                }
                else if LiveData["tournament_type"] as! String == "Match"
                {
                    var art1:Array<String> = LiveData["artists"]!.objectAtIndex(0) as! Array<String>
                    var art2:Array<String> = LiveData["artists"]!.objectAtIndex(1) as! Array<String>
                    
                    cell.Team1.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
                    cell.Team2.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
                    
                    cell.Team1.layer.cornerRadius=19
                    cell.Team1.layer.masksToBounds = true
                    
                    cell.Team2.layer.cornerRadius=19
                    cell.Team2.layer.masksToBounds = true
                }
                
                cell.NumUser.text=LiveData["totalbets"] as? String
            }
        }
        
        let selectView=UIView()
        selectView.backgroundColor=UIColor.blackColor()
        cell.selectedBackgroundView=selectView
        cell.backgroundColor=UIColor.clearColor()
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        if indexPath.row == 0
        {
            if selectCategory == indexPath.section
            {
                selectCategory = -1
            }
            else
            {
                selectCategory=indexPath.section
            }
            let transition = CATransition()
            transition.type = kCATransitionFade
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
            transition.fillMode = kCAFillModeForwards
            transition.duration = 0.8
            transition.subtype = kCATransitionFromTop
            self.OpenBetTable.layer.addAnimation(transition, forKey: "UITableViewReloadDataAnimationKey")
            OpenBetTable.reloadData()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setObject("OpenBet" , forKey: "DisplayBet")
            SelectSection = indexPath.section
            SelectRow = indexPath.row
            self.performSegueWithIdentifier("DisplayUserClicked", sender: self)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "DisplayUserClicked"
        {
            let secondViewController = (segue.destinationViewController as! DisplayUser)
            var idArr:Array<Int> = MatchId[SelectSection]
            let id:Int=idArr[SelectRow - 1]
            
            for _ in 0  ..< AllTournament.count
            {
                if let LiveData:Dictionary<String,AnyObject> = AllTournament[id]! as? Dictionary<String, AnyObject>
                {
                    secondViewController.LiveData=LiveData
                }
            }

        }
    }

}
