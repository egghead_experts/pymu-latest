//
//  MenuTableView.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class MenuTableView: UITableViewController
{
    var MenuCellIdentifier:Array<String> = []
    var MenuName:Array<String> = []
    var MenuImg:Array<String> = []

    override func viewDidLoad()
    {
        super.viewDidLoad()

        MenuCellIdentifier=["ProfileCell","CellHome", "CellFriends", "CellOpenBet", "CellInvitation", "CellChat", "CellSummery", "CellLogOut"]
        MenuImg=["","icon_home.png", "icon_usr_sch.png", "icon_open_bat.png", "icon_mail.png", "icon_chat.png", "icon_act_sum.png", "icon_logout.png"]
        MenuName=["","Home", "Friends", "Open Bet", "Invitation", "Chat", "Summary", "Log Out"]
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuTableView.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }

    override func viewWillAppear(animated: Bool)
    {
        self.tableView.reloadData()
    }
    func rotated()
    {
        self.tableView.reloadData()
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            //self.revealViewController().rearViewRevealWidth=(self.view.frame.width/5)*4
            //self.revealViewController().rearViewRevealOverdraw=0
            //self.revealViewController().rearViewRevealDisplacement=(self.view.frame.width/5)*4
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            //self.revealViewController().rearViewRevealWidth=(self.view.frame.width/5)*4
            //self.revealViewController().rearViewRevealOverdraw=0
            //self.revealViewController().rearViewRevealDisplacement=(self.view.frame.width/5)*4
        }
    }
   
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return MenuCellIdentifier.count
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 70
        }
        
        if UIScreen.mainScreen().bounds.size.height == 320.0
        {
            return 34
        }
        else if UIScreen.mainScreen().bounds.size.height == 375.0
        {
            return 40
        }
        
        return 44
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let identifier = "ProfileCell"
            let cell: ProfileCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? ProfileCell
            
            let picStr:String = (NSUserDefaults.standardUserDefaults().objectForKey("UserPic") as? String)!

            if let url = NSURL(string: picStr)
            {
                if let data = NSData(contentsOfURL: url)
                {
                    cell.ProfileImg.image = UIImage(data: data)
                }
            }
            cell.ProfileImg.layer.cornerRadius=27
            cell.ProfileImg.layer.masksToBounds = true
            
            let nameStr:String = (NSUserDefaults.standardUserDefaults().objectForKey("UserName") as? String)!
            cell.UserName.text=nameStr
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.clearColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()

            return cell
        }
        
        let cell: MenuCell! = tableView.dequeueReusableCellWithIdentifier(MenuCellIdentifier[indexPath.row]) as? MenuCell
     
        cell.MenuImg.image=UIImage(named:MenuImg[indexPath.row])
        
        let fnt=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 14))
        cell.MenuName.textColor=UIColor.whiteColor()
        cell.MenuName.font=fnt
        cell.MenuName.text=MenuName[indexPath.row]

        cell.backgroundColor=UIColor.clearColor()
        let selectView=UIView()
        selectView.backgroundColor=UIColor.blackColor()
        cell.selectedBackgroundView=selectView

        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
