//
//  Logout.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class Logout: UIViewController
{
    
    var ApiObj = PymuAPI()

    override func viewDidLoad()
    {
        super.viewDidLoad()

        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/logout/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if dataDict.object.objectForKey("msg") != nil
            {
                NSUserDefaults.standardUserDefaults().setObject("" , forKey: "token")
                NSUserDefaults.standardUserDefaults().setObject(0 , forKey: "keeplogin")
                
                let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("LoginView") as! LoginView
                self.navigationController!.pushViewController(secondViewController, animated: false)
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
    }

}
