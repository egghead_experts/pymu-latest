//
//  Chat.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class Chat: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var ChatView: UIView!
    @IBOutlet var Btn_Menu: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    
    @IBOutlet var ChatTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var AllFriendUser:Array<AnyObject> = []
    var AllFriendName:Array<AnyObject> = []
    
    var user_id = -1
    var user_name=""
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)

        Btn_Menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.ChatViewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.ChatViewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func ChatViewDidLoad()
    {
        ChatView.hidden=false
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/friendslist/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                AllFriendUser=[]
                AllFriendName=[]
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var user:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    
                    user["user_id"] = arr["user_id"]!.string!
                    user["user_name"] = arr["user_name"]!.string!
                    user["user_image"] = arr["user_image"]!.string!
                    user["status"] = arr["status"]!.string!
                    user["totalunread"] = arr["totalunread"]!.string!

                    AllFriendUser.insert(user, atIndex: AllFriendUser.count)
                    AllFriendName.insert(user, atIndex: AllFriendName.count)
                }
            }
        }
        ChatTable.reloadData()
    }
    
    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        searchBar.resignFirstResponder()
        Btn_BackMenu.hidden=false
    }
    
    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        let predicate=NSPredicate(format: "SELF.user_name CONTAINS[cd] %@", searchText)
        let arr=(AllFriendName as NSArray).filteredArrayUsingPredicate(predicate)
        
        if arr.count > 0
        {
            AllFriendUser.removeAll(keepCapacity: true)
            AllFriendUser=arr
        }
        else
        {
            AllFriendUser=AllFriendName
        }
        ChatTable.reloadData()
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar){}
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar){}
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        self.searchBar.resignFirstResponder()
    }
    
    // All Table Methods....
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return AllFriendUser.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 48
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let identifier = "UserCell"
        let cell: UserCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? UserCell
        
        let headerView=UIView()
        headerView.backgroundColor=UIColor(patternImage: UIImage(named:"user_ra_bg_i6.png")!)
        cell.backgroundView=headerView
        cell.backgroundColor=UIColor.clearColor()
        
        var Data:Dictionary<String,AnyObject> = AllFriendUser[indexPath.section] as! Dictionary<String, AnyObject>
        
        cell.imgUser.setImageWithUrl(NSURL(string:Data["user_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
        cell.imgUser.layer.cornerRadius=19
        
        if Data["status"] as! String == "active"
        {
            cell.imgFrm.image=UIImage(named:"user_act.png")
        }
        else if Data["status"] as! String == "inactive"
        {
            cell.imgFrm.image=UIImage(named:"user_dact.png")
        }
        
        cell.lblName.text=Data["user_name"] as? String
        
        if Int((Data["totalunread"] as? String)!)! == 0
        {
            cell.lblDoler.hidden=true
        }
        else
        {
            cell.lblDoler.hidden=false
        }
        cell.lblDoler.text=Data["totalunread"] as? String
        cell.Status.hidden=true
        
        let selectView=UIView()
        selectView.backgroundColor=UIColor.blackColor()
        cell.selectedBackgroundView=selectView
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        searchBar.resignFirstResponder()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var Data:Dictionary<String,AnyObject> = AllFriendUser[indexPath.section] as! Dictionary<String, AnyObject>
        user_id=Int(Data["user_id"]! as! String)!
        user_name=Data["user_name"] as! String
        self.performSegueWithIdentifier("GoChatting", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "GoChatting"
        {
            let sc = (segue.destinationViewController as! ChattingView)
            sc.user_id=self.user_id
            sc.user_name=self.user_name
        }
    }
}
