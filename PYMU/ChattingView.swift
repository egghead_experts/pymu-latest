//
//  ChattingView.swift
//  PYMU
//
//  Created by Egghead IOS3 on 21/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit

class ChattingView: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    var ApiObj = PymuAPI()
    
    @IBOutlet var Btn_Back_Chat: UIButton!
    @IBOutlet var ChattingUserName: UILabel!
    
    @IBOutlet var HistoryTable: UITableView!
    @IBOutlet var SendView: UIView!
    @IBOutlet var MessageTxt: UITextField!
    @IBOutlet var SendView_BottomSpace: NSLayoutConstraint!

    var user_id = -1
    var user_name=""

    var messages : Array<String> = []
    var type : Array<String> = []
    var timer:NSTimer!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ChattingUserName.text=user_name
        self.ChattingViewDidLoad()
        HistoryTable.reloadData()
        if HistoryTable.numberOfRowsInSection(0) > 0
        {
            let indexPath = NSIndexPath(forRow: messages.count - 1, inSection: 0)
            self.HistoryTable.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
        }
        
        MessageTxt.becomeFirstResponder()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(ChattingView.ChattingViewDidLoad), userInfo: nil, repeats: true)
        self.ChattingViewDidLoad()
    }
    
    func ChattingViewDidLoad()
    {
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("chat/history/\(urlStr)", postStr: "user_id=\(user_id)")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                messages = []
                type = []
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    messages.append(arr["message"]!.string!)
                    type.append(arr["type"]!.string!)
                }
                HistoryTable.reloadData()
            }
        }
    }
    
    @IBAction func Back_ChatView_Clicked(sender: AnyObject)
    {
        timer.invalidate()
        MessageTxt.resignFirstResponder()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func Done_Chatting_Clicked(sender: AnyObject)
    {
        timer.invalidate()
        MessageTxt.resignFirstResponder()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // All Table Methods....
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        let messageSize = PTSMessagingCell.messageSize(messages[indexPath.row])
        return messageSize.height + 2*PTSMessagingCell.textMarginVertical() + 5.0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = PTSMessagingCell(messagingCellWithReuseIdentifier:"cell")
        
        let headerView=UIView()
        headerView.backgroundColor=UIColor(patternImage: UIImage(named:"user_ra_bg_i6.png")!)
        cell.backgroundView=headerView
        cell.backgroundColor=UIColor.clearColor()
        
        if type[indexPath.row] == "sent"
        {
            cell.sent = true
            cell.messageLabel.textColor=UIColor.whiteColor()
        }
        else if type[indexPath.row] == "receive"
        {
            cell.sent = false
            cell.messageLabel.textColor=UIColor.blackColor()
        }
        cell.messageLabel.text = messages[indexPath.row]
        
        let selectView=UIView()
        selectView.backgroundColor=UIColor.blackColor()
        cell.selectedBackgroundView=selectView
        
        return cell
    }

    
    @IBAction func MsgSend_Btn_Clicked(sender: AnyObject)
    {
        if MessageTxt.text == ""
        {
            MessageTxt.resignFirstResponder()
            HistoryTable.reloadData()
            
            if HistoryTable.numberOfRowsInSection(0) > 0
            {
                let indexPath = NSIndexPath(forRow: messages.count - 1, inSection: 0)
                self.HistoryTable.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
            }
        }
        else
        {
            let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
            let postString = "user_id=\(user_id)&message=\(MessageTxt.text!)"
            var dataDict=ApiObj.CallPostApi("chat/sendmessage/\(urlStr)", postStr: postString)
            
            if (dataDict.null == nil)
            {
                if dataDict == 6
                {
                    TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
                }
                else if let _ = dataDict["msg"].string
                {
                    MessageTxt.text=""
                    self.ChattingViewDidLoad()
                }
            }
            
            HistoryTable.reloadData()
            if HistoryTable.numberOfRowsInSection(0) > 0
            {
                let indexPath = NSIndexPath(forRow: messages.count - 1, inSection: 0)
                self.HistoryTable.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        MessageTxt.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                if UIScreen.mainScreen().bounds.size.height == 480.0
                {
                    self.SendView_BottomSpace.constant=215
                }
                else if UIScreen.mainScreen().bounds.size.height == 568.0
                {
                    self.SendView_BottomSpace.constant=215
                }
                else if UIScreen.mainScreen().bounds.size.height == 667.0
                {
                    self.SendView_BottomSpace.constant=215
                }
                else if UIScreen.mainScreen().bounds.size.height == 736.0
                {
                    self.SendView_BottomSpace.constant=225
                }
                else if UIScreen.mainScreen().bounds.size.height == 1024.0
                {
                    self.SendView_BottomSpace.constant=315
                }
            },
            completion:
            {(value: Bool) in
        })
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        MessageTxt.resignFirstResponder()
        UIView.animateWithDuration(0.4, animations:
            {
                self.SendView_BottomSpace.constant=0
            },
            completion:
            {(value: Bool) in
        })
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        MessageTxt.resignFirstResponder()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
