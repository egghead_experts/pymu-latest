//
//  TInviteCell.swift
//  PYMU
//
//  Created by Egghead IOS3 on 10/03/16.
//  Copyright © 2016 egg. All rights reserved.
//

import UIKit

class TInviteCell: UITableViewCell
{
    @IBOutlet var Team1: UIImageView!
    @IBOutlet var Team1Frm: UIImageView!
    @IBOutlet var NextImg: UIImageView!
    
    @IBOutlet var NameUser: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
