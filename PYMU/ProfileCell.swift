//
//  ProfileCell.swift
//  PYMU
//
//  Created by Egghead IOS3 on 20/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell
{

    @IBOutlet var PofileFrm: UIImageView!
    @IBOutlet var ProfileImg: UIImageView!
    @IBOutlet var UserName: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
