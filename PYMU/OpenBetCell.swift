//
//  OpenBetCell.swift
//  PYMU
//
//  Created by Apple on 23/07/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class OpenBetCell: UITableViewCell
{

    @IBOutlet var Team1: UIImageView!
    @IBOutlet var Team1Frm: UIImageView!
    @IBOutlet var Team2: UIImageView!
    @IBOutlet var Team2Frm: UIImageView!
    
    @IBOutlet var Vs: UIImageView!
    @IBOutlet var NumUser: UILabel!

    
    @IBOutlet var TimeLeft: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

    }
}
