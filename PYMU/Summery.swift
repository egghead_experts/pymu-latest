//
//  Summery.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class Summery: UIViewController, UIGestureRecognizerDelegate , UITableViewDataSource , UITableViewDelegate
{
    var ApiObj = PymuAPI()

    @IBOutlet var SummeryView: UIView!
    @IBOutlet var Btn_Menu: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    
    @IBOutlet var BGImg_BetSumBtn: UIImageView!
    @IBOutlet var Btn_BetSum: UIButton!
    @IBOutlet var Btn_AccountSum: UIButton!
    
    @IBOutlet var BetSumTable: UITableView!
    @IBOutlet var AccountTable: UITableView!

    var BetSumData:Array<AnyObject> = []
    var currentamount:String="0"
    var winamount:String="0"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)

        Btn_Menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.SummeryViewDidLoad()
    }

    func setFrameX(frm:CGRect, x:CGFloat) -> CGRect
    {
        var frame=frm
        frame.origin.x = x
        return frame
    }
    
    func SummeryViewDidLoad()
    {
        SummeryView.hidden=false
        self.AccountTable.hidden=true

        self.BetSum_Btn_Clicked(self.Btn_BetSum)
        self.AccountTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(Summery.SummerySwipeGesture(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.BetSumTable.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(Summery.SummerySwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.AccountTable.addGestureRecognizer(swipeRight)
    }
    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=false
    }
    
    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    
    func SummerySwipeGesture(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case UISwipeGestureRecognizerDirection.Left:
                self.AccountSum_Btn_Clicked(Btn_AccountSum)
            case UISwipeGestureRecognizerDirection.Right:
                self.BetSum_Btn_Clicked(Btn_BetSum)
            default:
                break
            }
        }
    }
    
    @IBAction func BetSum_Btn_Clicked(sender: AnyObject)
    {
        BetSumTable.hidden=false

        UIView.animateWithDuration(0.4, animations:
            {
                self.BetSumTable.frame = self.setFrameX(self.BetSumTable.frame, x: 0)
                self.AccountTable.frame = self.setFrameX(self.AccountTable.frame, x: self.AccountTable.frame.size.width)
                self.BGImg_BetSumBtn.frame = self.setFrameX(self.BGImg_BetSumBtn.frame , x: self.Btn_BetSum.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.AccountTable.hidden=true
        })
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("betsummary/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                BetSumData=[]
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var data:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    
                    data["id"] = arr["id"]!.string!
                    data["user"] = arr["user"]!.string!
                    data["winlose"] = arr["winlose"]!.string!
                    data["tournament_type"] = arr["tournament_type"]!.string!
                    
                    if data["tournament_type"] as! String == "Tournament"
                    {
                        data["tournament_name"] = arr["tournament_name"]!.string!
                        data["tournament_image"] = arr["image"]!.string!
                    }
                    else if data["tournament_type"] as! String == "Match"
                    {
                        var teams = arr["artists"]!
                        
                        var team=teams[0].dictionaryValue
                        data["team1_image"] = team["image"]!.string!
                        team=teams[1].dictionaryValue
                        data["team2_image"] = team["image"]!.string!
                    }
                    BetSumData.append(data)
                }
            }
        }
        BetSumTable.reloadData()
        
        Btn_BetSum.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_BetSum.userInteractionEnabled=false
        Btn_AccountSum.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_AccountSum.userInteractionEnabled=true
    }
    
    @IBAction func AccountSum_Btn_Clicked(sender: AnyObject)
    {
        AccountTable.hidden=false
        self.AccountTable.frame = self.setFrameX(self.AccountTable.frame, x: self.AccountTable.frame.size.width)
        self.BetSumTable.frame = self.setFrameX(self.BetSumTable.frame, x: 0)
        
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.BetSumTable.frame = self.setFrameX(self.BetSumTable.frame, x: -(self.BetSumTable.frame.size.width))
                self.AccountTable.frame = self.setFrameX(self.AccountTable.frame, x: 0)
                self.BGImg_BetSumBtn.frame = self.setFrameX(self.BGImg_BetSumBtn.frame , x: self.Btn_AccountSum.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.BetSumTable.hidden=true
        })
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("accountsummary/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].dictionary
            {
                currentamount = result["currentamount"]!.string!
                winamount = result["winamount"]!.string!
            }
        }
        
        AccountTable.reloadData()
        
        Btn_AccountSum.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_AccountSum.userInteractionEnabled=false
        Btn_BetSum.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_BetSum.userInteractionEnabled=true
    }

    // All Table Methods....
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView.isEqual(BetSumTable)
        {
            return BetSumData.count
        }
        else if tableView.isEqual(AccountTable)
        {
            return 2
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.isEqual(AccountTable)
        {
            return 2
        }
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if tableView.isEqual(AccountTable)
        {
            if indexPath.row == 0
            {
                return 24
            }
            return 48
        }
        return 48
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView.isEqual(BetSumTable)
        {
            let cell: BetSumCell! = tableView.dequeueReusableCellWithIdentifier("BetSumCell") as? BetSumCell
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
            cell.backgroundView=headerView
            
            let Data:Dictionary<String,AnyObject> = BetSumData[indexPath.section] as! Dictionary<String, AnyObject>

            cell.NameUser.text=Data["user"] as? String
            
            if Data["winlose"] as! String == "win"
            {
                cell.Lose_WinImg.image=UIImage(named:"icon_win.png")
            }
            else if Data["winlose"] as! String == "lose"
            {
                cell.Lose_WinImg.image=UIImage(named:"icon_lose.png")
            }
            
            if Data["tournament_type"] as! String == "Tournament"
            {
                cell.TournamentImg.setImageWithUrl(NSURL(string:Data["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
                cell.TournamentImg.layer.cornerRadius=19
                cell.TournamentImg.hidden=false
                cell.TournamentFrm.hidden=false

                cell.Team1.hidden=true
                cell.Team1Frm.hidden=true
                cell.Team2.hidden=true
                cell.Team2Frm.hidden=true
                cell.vsImg.hidden=true
            }
            else if Data["tournament_type"] as! String == "Match"
            {
                cell.Team1.setImageWithUrl(NSURL(string:Data["team1_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
                cell.Team1.layer.cornerRadius=19
                cell.Team2.setImageWithUrl(NSURL(string:Data["team2_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
                cell.Team2.layer.cornerRadius=19
                
                cell.TournamentImg.hidden=true
                cell.TournamentFrm.hidden=true
            }
            
            cell.GameName.hidden=true
            cell.userInteractionEnabled=false
            cell.backgroundColor=UIColor.clearColor()
            return cell!
        }
        else if tableView.isEqual(AccountTable)
        {
            if indexPath.row == 0
            {
                let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
                
                let headerView=UIView()
                headerView.backgroundColor=UIColor(patternImage: UIImage(named:"catbg_i6.png")!)
                cell.backgroundView=headerView
                
                let fnt=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 13))
                let name:UILabel=UILabel(frame: CGRectMake(8, 2, 250, 20))
                name.textColor=UIColor.whiteColor()
                name.font=fnt
                
                if indexPath.section == 0
                {
                    name.text="Current Bet Detail"
                }
                else if indexPath.section == 1
                {
                    name.text="Won Bet Detail"
                }
                
                cell.addSubview(name)
                cell.userInteractionEnabled=false
                cell.backgroundColor=UIColor.clearColor()
                return cell
            }
            
            let identifier = "BetSumCell"
            let cell: BetSumCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? BetSumCell
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
            cell.backgroundView=headerView
            
            cell.GameName.frame=CGRectMake(260, 14, 50, 20)
            cell.GameName.font=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 16))
            
            cell.NameUser.frame.size.width=250
            cell.NameUser.font=UIFont(name: "AntartidaRoundedEssentiallight", size: CGFloat( 16))
            
            cell.Account_Name_Width.constant = -100
            if indexPath.section == 0
            {
                cell.NameUser.text="Your have Current Bet Ammount"
                cell.GameName.textColor=UIColor.whiteColor()
                cell.GameName.text="$ \(currentamount)"
            }
            else if indexPath.section == 1
            {
                cell.NameUser.text="Your have Won Bet Ammount"
                cell.GameName.textColor=UIColor.greenColor()
                cell.GameName.text="$ \(winamount)"
            }
            
            cell.Team1.hidden=true
            cell.Team1Frm.hidden=true
            cell.Team2.hidden=true
            cell.Team2Frm.hidden=true
            cell.vsImg.hidden=true
            cell.Lose_WinImg.hidden=true
            
            cell.userInteractionEnabled=false
            cell.backgroundColor=UIColor.clearColor()
            return cell!
        }
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        return cell
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
