//
//  LoginView.swift
//  PYMU
//
//  Created by Apple on 27/08/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class LoginView: UIViewController , UIAlertViewDelegate , UITextFieldDelegate, UIScrollViewDelegate
{
    // -*-*-*-*-*- Facebook Login -*-*-*-*-*-*-*-*-*-
    
    @IBOutlet var Btn_Facebook_Login: UIButton!
    var fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
    var HUD = MBProgressHUD()

    // -*-*-*-*-*- PYMU Login -*-*-*-*-*-*-*-*-*-*-*-
    
    var ApiObj = PymuAPI()
    
    @IBOutlet var BGImg_SignBtn: UIImageView!
    @IBOutlet var Btn_SignIn: UIButton!
    @IBOutlet var Btn_SignUp: UIButton!
    @IBOutlet var Btn_Login: UIButton!
    @IBOutlet var Btn_Forgot: UIButton!
    
    @IBOutlet var InView: UIView!
    @IBOutlet var email_IN: UITextField!
    @IBOutlet var password_IN: UITextField!
    @IBOutlet var Btn_KeepLogin: UIButton!
    
    @IBOutlet var UpView: UIView!
    @IBOutlet var firstName_UP: UITextField!
    @IBOutlet var lastName_UP: UITextField!
    @IBOutlet var email_UP: UITextField!
    @IBOutlet var password_UP: UITextField!
    @IBOutlet var conformPass_UP: UITextField!
    @IBOutlet var Btn_Terms_Condition: UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
    
    @IBOutlet var SignUp_BottomSpace: NSLayoutConstraint!
    
    @IBOutlet var InText_Height: [NSLayoutConstraint]!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if (NSUserDefaults.standardUserDefaults().objectForKey("keeplogin")) == nil
        {
            NSUserDefaults.standardUserDefaults().setObject(0 , forKey: "keeplogin")
            NSUserDefaults.standardUserDefaults().setObject("" , forKey: "UserName")
            NSUserDefaults.standardUserDefaults().setObject("" , forKey: "UserPic")
            NSUserDefaults.standardUserDefaults().setObject("" , forKey: "token")
            
            NSUserDefaults.standardUserDefaults().setObject("Home" , forKey: "BettingView")
            NSUserDefaults.standardUserDefaults().setObject("Invitation" , forKey: "DisplayBet")
            
            NSUserDefaults.standardUserDefaults().setObject("" , forKey: "coinbase")

        }
        
        if (NSUserDefaults.standardUserDefaults().objectForKey("keeplogin"))! as! Int == 1
        {
            let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        else if (NSUserDefaults.standardUserDefaults().objectForKey("keeplogin"))! as! Int == 0
        {
            self.LoginViewLoad()
        }
    }
    
    func LoginViewLoad()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginView.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()

        self.SignIn_Clicked(Btn_SignIn)
        self.setAllTextFieldPlaceholder()
        
        if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            for i in InText_Height
            {
                i.constant=40
            }
        }
    }

    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
    }
    
    // -------------------------------------------
    // -*-*-*-*-*- Facebook Login *-*-*-*-*-*-*-*-
    // -------------------------------------------
    
    @IBAction func Facebook_Login_Clicked(sender: AnyObject)
    {
        HUD=MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        HUD.labelText="Loading"
        HUD.dimBackground=true
        
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.SystemAccount

        fbLoginManager .logInWithReadPermissions(["public_profile","email", "user_friends"], handler: { (result, error) -> Void in
            if (error == nil)
            {
                let fbloginresult : FBSDKLoginManagerLoginResult = result
                if((FBSDKAccessToken.currentAccessToken()) != nil)
                {
                    if (fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                    else
                    {
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    }
                }
                else
                {
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }
            }
            else
            {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }
        })
    }
    
    func getFBUserData()
    {
        if((FBSDKAccessToken.currentAccessToken()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,friends"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil)
                {
                    let fName:String = result["first_name"] as! String
                    let lName:String = result["last_name"] as! String
                    let Email:String = result["email"] as! String
                    let ID:String = result["id"] as! String
                    //var picture = result["picture"] as! Dictionary<String,AnyObject>
                    //var pic = picture["data"] as! Dictionary<String,AnyObject>
                    //var img = pic["url"] as! String
                    
                    let postString = "fbid=\(ID)&first_name=\(fName)&last_name=\(lName)&email=\(Email)";
                    var dataDict=self.ApiObj.CallPostApi("user/login", postStr: postString)
                    
                    if (dataDict.null == nil)
                    {
                        if dataDict == 6
                        {
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: true, callback: { () -> Void in})
                        }
                        else if dataDict.object.objectForKey("token") != nil
                        {
                            print("User Token :- "+dataDict["token"].string!)
                            
                            NSUserDefaults.standardUserDefaults().setObject(dataDict["token"].string , forKey: "token")
                            NSUserDefaults.standardUserDefaults().setObject(1 , forKey: "keeplogin")
                            NSUserDefaults.standardUserDefaults().setObject("\(dataDict["first_name"].string!) \(dataDict["last_name"].string!)" , forKey: "UserName")
                            NSUserDefaults.standardUserDefaults().setObject(dataDict["profilepic"].string! , forKey: "UserPic")

                            self.SignIn_Clicked(self.Btn_SignIn)
                            self.performSegueWithIdentifier("SignInClicked", sender: self)
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                        }
                        else
                        {
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            print(dataDict["err"])
                        }
                    }
                }
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            })
        }
    }
    

    // -------------------------------------------
    // -*-*-*-*-*- PYMU Login *-*-*-*-*-*-*-*-*-*-
    // -------------------------------------------
    
    func setAllTextFieldPlaceholder()
    {
        email_IN.attributedPlaceholder = NSAttributedString(string:"E-mail address", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        password_IN.attributedPlaceholder = NSAttributedString(string:"Password", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        firstName_UP.attributedPlaceholder = NSAttributedString(string:"First Name", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        lastName_UP.attributedPlaceholder = NSAttributedString(string:"Last Name", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        email_UP.attributedPlaceholder = NSAttributedString(string:"E-mail address", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        password_UP.attributedPlaceholder = NSAttributedString(string:"Password", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        conformPass_UP.attributedPlaceholder = NSAttributedString(string:"Re-Password", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
    }
    
    func setAllTextFieldText()
    {
        email_IN.text = ""
        password_IN.text = ""
        firstName_UP.text = ""
        lastName_UP.text = ""
        email_UP.text = ""
        password_UP.text = ""
        conformPass_UP.text = ""
    }
    
    func setAllTextFieldKeyBoardHidden()
    {
        email_IN.resignFirstResponder()
        password_IN.resignFirstResponder()
        email_UP.resignFirstResponder()
        password_UP.resignFirstResponder()
        firstName_UP.resignFirstResponder()
        lastName_UP.resignFirstResponder()
        conformPass_UP.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if UIScreen.mainScreen().bounds.size.height == 667.0
        {
            if textField.isEqual(conformPass_UP) || textField.isEqual(password_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -85
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 568.0
        {
            if textField.isEqual(conformPass_UP) || textField.isEqual(password_UP) || textField.isEqual(email_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -105
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            if textField.isEqual(lastName_UP) || textField.isEqual(email_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -80
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
            if textField.isEqual(conformPass_UP) || textField.isEqual(password_UP) || textField.isEqual(email_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = -103
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if UIScreen.mainScreen().bounds.size.height == 667.0
        {
            if textField.isEqual(conformPass_UP) || textField.isEqual(password_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 568.0
        {
            if textField.isEqual(conformPass_UP) || textField.isEqual(password_UP) || textField.isEqual(email_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
        else if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            if textField.isEqual(lastName_UP) || textField.isEqual(email_UP) || textField.isEqual(conformPass_UP) || textField.isEqual(password_UP) || textField.isEqual(email_UP)
            {
                UIView.animateWithDuration(0.4, animations:
                    {
                        var frame=self.view.frame
                        frame.origin.y = 0
                        self.view.frame = frame
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField.isEqual(firstName_UP)
        {
            lastName_UP.becomeFirstResponder()
        }
        else if textField.isEqual(lastName_UP)
        {
            email_UP.becomeFirstResponder()
        }
        else if textField.isEqual(email_UP)
        {
            password_UP.becomeFirstResponder()
        }
        else if textField.isEqual(password_UP)
        {
            conformPass_UP.becomeFirstResponder()
        }
        else if textField.isEqual(conformPass_UP)
        {
            textField.resignFirstResponder()
        }
        else if textField.isEqual(email_IN)
        {
            password_IN.becomeFirstResponder()
        }
        else if textField.isEqual(password_IN)
        {
            textField.resignFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.setAllTextFieldKeyBoardHidden()
    }
    
    func setFrameX(frm:CGRect, x:CGFloat) -> CGRect
    {
        var frame=frm
        frame.origin.x=x
        return frame
    }
    
    @IBAction func SignIn_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.BGImg_SignBtn.frame = self.setFrameX(self.BGImg_SignBtn.frame , x: 0)
            },
            completion:
            {(value: Bool) in
        })
        
        InView.hidden=false
        UpView.hidden=true
        Btn_Login.enabled=true
        Btn_Forgot.hidden=false
        Btn_Facebook_Login.hidden=false
        
        self.setAllTextFieldText()
        self.setAllTextFieldKeyBoardHidden()

        Btn_SignIn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_SignIn.setBackgroundImage(UIImage(named: "btn_act_tab.png"), forState: UIControlState.Normal)
        Btn_SignIn.userInteractionEnabled=false
        
        Btn_SignUp.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_SignUp.setBackgroundImage(UIImage(named: "btn_dact_tab.png"), forState: UIControlState.Normal)
        Btn_SignUp.userInteractionEnabled=true
       
        Btn_Login.setTitle("SIGN IN", forState: UIControlState.Normal)
        Btn_Login.tag=11
        Btn_KeepLogin.tag=1
        self.KeepLogin_Clicked(Btn_KeepLogin)
        Btn_Login.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            SignUp_BottomSpace.constant=0
        }
    }
    
    @IBAction func SignUp_Clicked(sender: AnyObject)
    {

        UIView.animateWithDuration(0.4, animations:
            {
                self.BGImg_SignBtn.frame = self.setFrameX(self.BGImg_SignBtn.frame , x: self.BGImg_SignBtn.frame.size.width)
            },
            completion:
            {(value: Bool) in
        })
        InView.hidden=true
        UpView.hidden=false
        Btn_Login.enabled=false
        Btn_Forgot.hidden=true
        Btn_Facebook_Login.hidden=true
        
        self.setAllTextFieldText()
        self.setAllTextFieldKeyBoardHidden()
        firstName_UP.becomeFirstResponder()
        
        Btn_SignUp.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_SignUp.setBackgroundImage(UIImage(named: "btn_act_tab"), forState: UIControlState.Normal)
        Btn_SignUp.userInteractionEnabled=false
        
        Btn_SignIn.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_SignIn.setBackgroundImage(UIImage(named: "btn_dact_tab"), forState: UIControlState.Normal)
        Btn_SignIn.userInteractionEnabled=true
        
        Btn_Login.setTitle("SIGN UP", forState: UIControlState.Normal)
        Btn_Login.tag=22
        Btn_Terms_Condition.tag=1
        self.Terms_Condition_Clicked(Btn_Terms_Condition)
        
        if UIScreen.mainScreen().bounds.size.height == 480.0
        {
            SignUp_BottomSpace.constant = -25
        }
    }
    
    @IBAction func KeepLogin_Clicked(sender: AnyObject)
    {
        if Btn_KeepLogin.tag == 0
        {
            Btn_KeepLogin.tag=1
            Btn_KeepLogin.setImage(UIImage(named: "checkMark_act.png"), forState: UIControlState.Normal)
        }
        else if Btn_KeepLogin.tag == 1
        {
            Btn_KeepLogin.setImage(UIImage(named: "checkMark.png"), forState: UIControlState.Normal)
            Btn_KeepLogin.tag=0
        }
    }
    
    
    @IBAction func Login_Clicked(sender: AnyObject)
    {
        let myBtn: AnyObject=sender
        
        if (myBtn.tag == 11)
        {
            var msg:String = ""
            if email_IN.text!.isEmpty
            {
                msg="Please enter email"
            }
            else if password_IN.text!.isEmpty
            {
                msg="Please enter password"
            }
            else
            {
                let postString = "uid=\(email_IN.text!)&pwd=\(password_IN.text!)"
                var dataDict=ApiObj.CallPostApi("user/login", postStr: postString)
                
                if (dataDict.null == nil)
                {
                    if dataDict == 6
                    {
                        TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
                    }
                    else if dataDict.object.objectForKey("token") != nil
                    {
                        if Btn_KeepLogin.tag == 0
                        {
                            NSUserDefaults.standardUserDefaults().setObject(0 , forKey: "keeplogin")
                        }
                        else if Btn_KeepLogin.tag == 1
                        {
                            NSUserDefaults.standardUserDefaults().setObject(1 , forKey: "keeplogin")
                        }
                        
                        NSUserDefaults.standardUserDefaults().setObject(dataDict["token"].string , forKey: "token")
                        NSUserDefaults.standardUserDefaults().setObject("\(dataDict["first_name"].string!) \(dataDict["last_name"].string!)" , forKey: "UserName")
                        NSUserDefaults.standardUserDefaults().setObject(dataDict["profilepic"].string! , forKey: "UserPic")
                        print("User Token :- "+dataDict["token"].string!)
                        
                        performSegueWithIdentifier("SignInClicked", sender: sender)
                        
                        self.setAllTextFieldText()
                        msg = ""
                    }
                    else if dataDict.object.objectForKey("err") != nil
                    {
                        password_IN.text=""
                        msg = "Invalid User/Password"
                    }
                }
            }
            
            if msg != ""
            {
                let alertController = UIAlertController(title: "PYMU", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        else if (myBtn.tag == 22)
        {
            
            var msg:String = ""
            let pass=password_UP.text
            let conformPass=conformPass_UP.text
            if firstName_UP.text!.isEmpty
            {
                msg="Please enter first name"
            }
            else if lastName_UP.text!.isEmpty
            {
                msg="Please enter last name"
            }
            else if email_UP.text!.isEmpty
            {
                msg="Please enter email"
            }
            else if password_UP.text!.isEmpty
            {
                msg="Please enter password"
            }
            else if conformPass_UP.text!.isEmpty
            {
                msg="Please enter conform password"
            }
            else if pass != conformPass
            {
                msg="Conform password not match"
            }
            else
            {
                let postString = "first_name=\(firstName_UP.text!)&last_name=\(lastName_UP.text!)&email=\(email_UP.text!)&pwd=\(password_UP.text!)";
                var dataDict=ApiObj.CallPostApi("user/register", postStr: postString)
                
                if (dataDict.null == nil)
                {
                    if dataDict == 6
                    {
                        TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
                    }
                    else if dataDict.object.objectForKey("msg") != nil
                    {
                        msg="User registered successfully"
                        self.setAllTextFieldPlaceholder()
                        self.SignIn_Clicked(Btn_SignIn)
                        
                        self.setAllTextFieldText()
                    }
                    else if dataDict.object.objectForKey("err") != nil
                    {
                        password_UP.text=""
                        conformPass_UP.text=""
                        msg="User already exists with \(email_UP.text) email address"
                    }
                }
            }
            
            let alertController = UIAlertController(title: "PYMU", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func Forgot_Clicked(sender: AnyObject)
    {
        performSegueWithIdentifier("ForgotClicked", sender: sender)
    }
    
    @IBAction func Terms_Condition_Clicked(sender: AnyObject)
    {
        if Btn_Terms_Condition.tag == 0
        {
            Btn_Terms_Condition.tag=1
            Btn_Terms_Condition.setImage(UIImage(named: "checkMark_act.png"), forState: UIControlState.Normal)
            Btn_Login.enabled=true
            Btn_Login.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        }
        else if Btn_Terms_Condition.tag == 1
        {
            Btn_Terms_Condition.tag=0
            Btn_Terms_Condition.setImage(UIImage(named: "checkMark.png"), forState: UIControlState.Normal)
            Btn_Login.enabled=false
            Btn_Login.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        }
    }
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
