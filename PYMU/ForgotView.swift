//
//  ForgotView.swift
//  PYMU Smart
//
//  Created by Apple on 13/08/15.
//  Copyright (c) 2015 egg. All rights reserved.
//

import UIKit

class ForgotView: UIViewController, UITextFieldDelegate, UIAlertViewDelegate, UIScrollViewDelegate
{
    var ApiObj = PymuAPI()

    @IBOutlet var Btn_ResetPass: UIButton!
    @IBOutlet var email_Forgot: UITextField!
    
    @IBOutlet var PopupView: UIView!
    @IBOutlet var AlertView: UIView!

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ContentView_Height: NSLayoutConstraint!
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        email_Forgot.attributedPlaceholder = NSAttributedString(string:"E-mail address", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        PopupView.hidden=true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ForgotView.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.rotated()
    }
    
    func rotated()
    {
        scrollView.delegate=self
        scrollView.contentSize.height = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
        ContentView_Height.constant = (UIScreen.mainScreen().bounds.size.height > UIScreen.mainScreen().bounds.size.width) ? UIScreen.mainScreen().bounds.size.height :UIScreen.mainScreen().bounds.size.width
    }

    func setFrameX(frm:CGRect, x:CGFloat) -> CGRect
    {
        var frame=frm
        frame.origin.x=x
        return frame
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        email_Forgot.resignFirstResponder()
    }

    @IBAction func Forgot_Back_Btn_Clicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func ResetPass_Clicked(sender: AnyObject)
    {
        if email_Forgot.text!.isEmpty
        {
            let alertController = UIAlertController(title: "PYMU", message: "Please enter email", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else
        {
            let postString = "uid=\(email_Forgot.text!)"
            var dataDict=ApiObj.CallPostApi("user/forgetpassword", postStr: postString)
            
            if (dataDict.null == nil)
            {
                if dataDict == 6
                {
                    TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
                }
                else if dataDict.object.objectForKey("err") != nil
                {
                    let msg=dataDict["err"].string
                    let alertController = UIAlertController(title: "PYMU", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
                else if dataDict.object.objectForKey("msg") != nil
                {
                    self.PopupView.frame = self.setFrameX(self.PopupView.frame, x: 0)
                    UIView.animateWithDuration(0.4, animations:
                        {
                            self.PopupView.hidden=false
                            self.AlertView.layer.masksToBounds=true
                            self.PopupView.alpha=0.7
                            self.PopupView.alpha=1.0
                        },
                        completion:
                        {(value: Bool) in
                    })
                }
            }
        }
    }
    
    @IBAction func OK_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.email_Forgot.text=""
            },
            completion:
            {(value: Bool) in
        })
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
