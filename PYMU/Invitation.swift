//
//  Invitation.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class Invitation: UIViewController, UITableViewDataSource , UITableViewDelegate
{
    var ApiObj = PymuAPI()

    @IBOutlet var InvitationView: UIView!
    @IBOutlet var Btn_Menu: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    @IBOutlet var InvitationTable: UITableView!
 
    var AllInvitation:Array<AnyObject> = []
    var SelectRow:Int = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)

        Btn_Menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }

    override func viewDidAppear(animated: Bool)
    {
        self.InvitationViewDidLoad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func InvitationViewDidLoad()
    {

        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/invitations/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                AllInvitation=[]

                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var invate:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()
                    
                    invate["bet_id"] = arr["bet_id"]!.string!
                    invate["user_name"] = arr["user_name"]!.string!

                    invate["tournament_type"] = arr["tournament_type"]!.string!
                    invate["tournament_name"] = arr["tournament_name"]!.string!
                    invate["tournament_image"] = arr["tournament_image"]!.string!
                    
                    invate["bet_amount"] = arr["bet_amount"]!.string!
                    invate["bet_for"]=arr["bet_for"]!.string!
                    invate["points"]=arr["points"]!.string!
        
                    invate["artist_id"] = arr["artist_id"]!.string!
                    invate["artist_name"] = arr["artist_name"]!.string!
                    invate["artist_image"] = arr["artist_image"]!.string!

                    var teams = arr["artists"]!
                    var artists:Array<AnyObject> = Array<AnyObject>()
                    for art in 0  ..< teams.count
                    {
                        var team=teams[art].dictionaryValue
                        
                        var tem:Array<String> = Array<String>()
                        tem.append(team["artist_id"]!.string!)
                        tem.append(team["artist_name"]!.string!)
                        tem.append(team["artist_image"]!.string!)
                        
                        artists.append(tem)
                    }
                    invate["artists"]=artists
        
                    AllInvitation.append(invate)
                }
            }
        }
        InvitationTable.reloadData()
    }
    
    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=false
    }
    
    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    

    // All Table Methods....
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return AllInvitation.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 48
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var invate:Dictionary<String,AnyObject> = AllInvitation[indexPath.section] as! Dictionary<String, AnyObject>
        if invate["tournament_type"] as! String == "Tournament"
        {
            let cell: TInviteCell! = tableView.dequeueReusableCellWithIdentifier("TInviteCell") as? TInviteCell
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
            cell.backgroundView=headerView
            
            cell.Team1.setImageWithUrl(NSURL(string:invate["tournament_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
            cell.Team1.layer.cornerRadius=19
            
            cell.NameUser.frame.size.width=90
            cell.NameUser.text=invate["user_name"] as? String
            
            cell.NextImg.image=UIImage(named:"icon_next.png")
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.blackColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()
            
            return cell!
        }
        else if invate["tournament_type"] as! String == "Match"
        {
            let cell: InviteCell! = tableView.dequeueReusableCellWithIdentifier("InviteCell") as? InviteCell
            
            let headerView=UIView()
            headerView.backgroundColor=UIColor(patternImage: UIImage(named:"ra_bg_i6.png")!)
            cell.backgroundView=headerView
            
            var art1:Array<String> = invate["artists"]!.objectAtIndex(0) as! Array<String>
            var art2:Array<String> = invate["artists"]!.objectAtIndex(1) as! Array<String>

            cell.Team1.setImageWithUrl(NSURL(string:art1[2])!, placeHolderImage: UIImage(named:"team_bg"))
            cell.Team2.setImageWithUrl(NSURL(string:art2[2])!, placeHolderImage: UIImage(named:"team_bg"))
            
            cell.Team1.layer.cornerRadius=19
            cell.Team2.layer.cornerRadius=19
            
            cell.NameUser.frame.size.width=90
            cell.NameUser.text=invate["user_name"] as? String
            
            cell.NextImg.image=UIImage(named:"icon_next.png")
            
            let selectView=UIView()
            selectView.backgroundColor=UIColor.blackColor()
            cell.selectedBackgroundView=selectView
            cell.backgroundColor=UIColor.clearColor()
                
            return cell!
        }
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        NSUserDefaults.standardUserDefaults().setObject("Invitation" , forKey: "DisplayBet")

        SelectRow=indexPath.section
        var invate:Dictionary<String,AnyObject> = AllInvitation[indexPath.section] as! Dictionary<String, AnyObject>
        if invate["tournament_type"] as! String == "Tournament"
        {
            self.performSegueWithIdentifier("TournamentInvite", sender: self)
        }
        else if invate["tournament_type"] as! String == "Match"
        {
            self.performSegueWithIdentifier("MatchInvite", sender: self)
        }
    }


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "MatchInvite"
        {
            let sv=(segue.destinationViewController as! DisplayMatch)
            sv.LiveData=self.AllInvitation[SelectRow] as! Dictionary<String, AnyObject> 
        }
        else if segue.identifier == "TournamentInvite"
        {
            let sv = (segue.destinationViewController as! DisplayTournament)
            sv.LiveData=self.AllInvitation[SelectRow] as! Dictionary<String, AnyObject>
        }
    }

}
