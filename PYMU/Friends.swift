//
//  Friends.swift
//  PYMU
//
//  Created by Egghead IOS3 on 19/10/15.
//  Copyright © 2015 egg. All rights reserved.
//

import UIKit

class Friends: UIViewController, UIGestureRecognizerDelegate , UITableViewDataSource , UITableViewDelegate
{
    var ApiObj = PymuAPI()

    @IBOutlet var FriendsView: UIView!
    @IBOutlet var Btn_Menu: UIButton!
    @IBOutlet var Btn_BackMenu: UIButton!
    @IBOutlet var BGImg_RequestBtn: UIImageView!
    @IBOutlet var Btn_FindFriend: UIButton!
    @IBOutlet var Btn_Request: UIButton!
    @IBOutlet var Btn_MyFriend: UIButton!
    
    @IBOutlet var FindFriendView: UIView!
    @IBOutlet var FindFriendsTable: UITableView!
    @IBOutlet var FindFriendsSB: UISearchBar!
    
    @IBOutlet var RequestView: UIView!
    @IBOutlet var RequestTable: UITableView!
    
    @IBOutlet var MyFriendView: UIView!
    @IBOutlet var MyFriendTable: UITableView!
    @IBOutlet var MyFriendSB: UISearchBar!
    
    @IBOutlet var FriendProfileView: UIView!
    @IBOutlet var Btn_Back_Profile: UIButton!
    @IBOutlet var Btn_Invite_Friend: UIButton!
    
    @IBOutlet var FriendPic: UIImageView!
    @IBOutlet var FriendFrm: UIImageView!
    @IBOutlet var Name: UITextField!
    
    @IBOutlet var Email: UITextField!
    @IBOutlet var NumBet: UITextField!
    
    @IBOutlet var PopView: UIView!
    @IBOutlet var AlrtView: UIView!
    @IBOutlet var AlrtMessage: UILabel!
    
    var AllFindUser:Array<AnyObject> = []
    var AllFindName:Array<AnyObject> = []
    
    var AllRequestUser:Array<AnyObject> = []
    
    var AllFriendUser:Array<AnyObject> = []
    var AllFriendName:Array<AnyObject> = []
    
    var seleted_id = -1
    var seleted_name=""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Btn_BackMenu.hidden=true
        self.view.bringSubviewToFront(Btn_BackMenu)

        Btn_Menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.FriendViewDidLoad()
    }

    @IBAction func Menu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=false
        self.FindFriendsSB.resignFirstResponder()
        self.MyFriendSB.resignFirstResponder()
    }
    
    @IBAction func BackMenu_Btn_Clicked(sender: AnyObject)
    {
        Btn_BackMenu.hidden=true
        self.revealViewController().revealToggleAnimated(true)
    }
    
    func setFrameX(frm:CGRect, x:CGFloat) -> CGRect
    {
        var frame=frm
        frame.origin.x = x
        return frame
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.FindFriendsSB.resignFirstResponder()
        self.MyFriendSB.resignFirstResponder()
    }
    
    func FriendViewDidLoad()
    {
        FriendsView.hidden=false
        RequestView.hidden=true
        MyFriendView.hidden=true
        FriendProfileView.hidden=true
        PopView.hidden=true
        self.view.bringSubviewToFront(FriendProfileView)
        self.view.bringSubviewToFront(PopView)
        
        self.FindFriends_Btn_Clicked(self.Btn_FindFriend)
        
        let FindFriendsLeft = UISwipeGestureRecognizer(target: self, action: #selector(Friends.FriendsSwipeGesture(_:)))
        FindFriendsLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.FindFriendsTable.addGestureRecognizer(FindFriendsLeft)
        
        let FindFriendsRight = UISwipeGestureRecognizer(target: self, action: #selector(Friends.FriendsSwipeGesture(_:)))
        FindFriendsRight.direction = UISwipeGestureRecognizerDirection.Right
        self.FindFriendsTable.addGestureRecognizer(FindFriendsRight)
        
        let RequestLeft = UISwipeGestureRecognizer(target: self, action: #selector(Friends.FriendsSwipeGesture(_:)))
        RequestLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.RequestTable.addGestureRecognizer(RequestLeft)
        
        let RequestRight = UISwipeGestureRecognizer(target: self, action: #selector(Friends.FriendsSwipeGesture(_:)))
        RequestRight.direction = UISwipeGestureRecognizerDirection.Right
        self.RequestTable.addGestureRecognizer(RequestRight)
        
        let MyFriendLeft = UISwipeGestureRecognizer(target: self, action: #selector(Friends.FriendsSwipeGesture(_:)))
        MyFriendLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.MyFriendTable.addGestureRecognizer(MyFriendLeft)
        
        let MyFriendRight = UISwipeGestureRecognizer(target: self, action: #selector(Friends.FriendsSwipeGesture(_:)))
        MyFriendRight.direction = UISwipeGestureRecognizerDirection.Right
        self.MyFriendTable.addGestureRecognizer(MyFriendRight)
    }
    
    func FriendsSwipeGesture(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case UISwipeGestureRecognizerDirection.Left:
                if Btn_FindFriend.tag == 1
                {
                    self.Request_Btn_Clicked(Btn_Request)
                }
                else if Btn_Request.tag == 1
                {
                    self.MyFriend_Btn_Clicked(Btn_MyFriend)
                }
                else if Btn_MyFriend.tag == 1
                {}
            case UISwipeGestureRecognizerDirection.Right:
                if Btn_MyFriend.tag == 1
                {
                    self.Request_Btn_Clicked(Btn_Request)
                }
                else if Btn_Request.tag == 1
                {
                    self.FindFriends_Btn_Clicked(Btn_FindFriend)
                }
                else if Btn_FindFriend.tag == 1
                {}
            default:
                break
            }
        }
    }
    
    @IBAction func FindFriends_Btn_Clicked(sender: AnyObject)
    {
        FindFriendView.hidden=false
        if Btn_Request.tag == 1
        {
            self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: -(self.FindFriendView.frame.size.width))
            self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: 0)
            self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: self.MyFriendView.frame.size.width)
        }
        else if Btn_MyFriend.tag == 1
        {
            self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: -(self.FindFriendView.frame.size.width))
            self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: self.RequestView.frame.size.width)
            self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: 0)
        }
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: 0)
                self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: self.RequestView.frame.size.width)
                self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: self.MyFriendView.frame.size.width)
                self.BGImg_RequestBtn.frame = self.setFrameX(self.BGImg_RequestBtn.frame , x: self.Btn_FindFriend.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.RequestView.hidden=true
                self.MyFriendView.hidden=true
        })
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/findfriends/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            AllFindUser=[]
            AllFindName=[]
            
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                for i in 0  ..< result.count
                {
                    var arr=result[i].dictionaryValue
                    var user:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    
                    user["user_id"] = arr["user_id"]!.string!
                    user["user_name"] = arr["user_name"]!.string!
                    user["email"] = arr["email"]!.string!
                    user["totalbets"] = arr["totalbets"]!.string!
                    user["user_image"] = arr["user_image"]!.string!

                    AllFindUser.insert(user, atIndex: AllFindUser.count)
                    AllFindName.insert(user, atIndex: AllFindName.count)
                }
            }
        }
        FindFriendsTable.reloadData()

        Btn_FindFriend.tag = 1
        Btn_Request.tag = 0
        Btn_MyFriend.tag = 0
        
        Btn_FindFriend.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_FindFriend.userInteractionEnabled=false
        Btn_Request.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_Request.userInteractionEnabled=true
        Btn_MyFriend.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_MyFriend.userInteractionEnabled=true
        
        self.FindFriendsSB.resignFirstResponder()
        self.MyFriendSB.resignFirstResponder()
    }
    
    @IBAction func Request_Btn_Clicked(sender: AnyObject)
    {
        RequestView.hidden=false
        
        if Btn_FindFriend.tag == 1
        {
            self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: 0)
            self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: self.RequestView.frame.size.width)
            self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: self.MyFriendView.frame.size.width)
        }
        else if Btn_MyFriend.tag == 1
        {
            self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: -(self.FindFriendView.frame.size.width))
            self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: -(self.RequestView.frame.size.width))
            self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: 0)
        }
        
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: -(self.FindFriendView.frame.size.width))
                self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: 0)
                self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: self.MyFriendView.frame.size.width)
                self.BGImg_RequestBtn.frame = self.setFrameX(self.BGImg_RequestBtn.frame , x: self.Btn_Request.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.FindFriendView.hidden=true
                self.MyFriendView.hidden=true
        })
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/requestlist/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            AllRequestUser=[]

            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                for i in 0  ..< result.count
                {
                    var arr=result[i].dictionaryValue
                    var user:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    
                    user["user_id"] = arr["user_id"]!.string!
                    user["user_name"] = arr["user_name"]!.string!
                    user["email"] = arr["email"]!.string!
                    user["totalbets"] = arr["totalbets"]!.string!
                    user["user_image"] = arr["user_image"]!.string!
                    
                    AllRequestUser.insert(user, atIndex: AllRequestUser.count)
                }
            }
        }
        RequestTable.reloadData()
        
        Btn_FindFriend.tag = 0
        Btn_Request.tag = 1
        Btn_MyFriend.tag = 0
        
        Btn_FindFriend.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_FindFriend.userInteractionEnabled=true
        Btn_Request.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_Request.userInteractionEnabled=false
        Btn_MyFriend.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_MyFriend.userInteractionEnabled=true
        
        self.FindFriendsSB.resignFirstResponder()
        self.MyFriendSB.resignFirstResponder()
    }
    
    @IBAction func MyFriend_Btn_Clicked(sender: AnyObject)
    {
        MyFriendView.hidden=false
        
        if Btn_FindFriend.tag == 1
        {
            self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: 0)
            self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: -(self.RequestView.frame.size.width))
            self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: self.MyFriendView.frame.size.width)
        }
        else if Btn_Request.tag == 1
        {
            self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: -(self.FindFriendView.frame.size.width))
            self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: 0)
            self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: self.MyFriendView.frame.size.width)
        }
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.FindFriendView.frame = self.setFrameX(self.FindFriendView.frame, x: -(self.FindFriendView.frame.size.width))
                self.RequestView.frame = self.setFrameX(self.RequestView.frame, x: -(self.RequestView.frame.size.width))
                self.MyFriendView.frame = self.setFrameX(self.MyFriendView.frame, x: 0)
                self.BGImg_RequestBtn.frame = self.setFrameX(self.BGImg_RequestBtn.frame , x: self.Btn_MyFriend.frame.origin.x)
            },
            completion:
            {(value: Bool) in
                self.FindFriendView.hidden=true
                self.RequestView.hidden=true
        })
        
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/friendslist/\(urlStr)", postStr: "")
        
        if (dataDict.null == nil)
        {
            AllFriendUser=[]
            AllFriendName=[]
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let result = dataDict["result"].array
            {
                for i in 0  ..< result.count 
                {
                    var arr=result[i].dictionaryValue
                    var user:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                    
                    user["user_id"] = arr["user_id"]!.string!
                    user["user_name"] = arr["user_name"]!.string!
                    user["email"] = arr["email"]!.string!
                    user["totalbets"] = arr["totalbets"]!.string!
                    user["user_image"] = arr["user_image"]!.string!
                    
                    AllFriendUser.insert(user, atIndex: AllFriendUser.count)
                    AllFriendName.insert(user, atIndex: AllFriendName.count)
                }
            }
        }
        MyFriendTable.reloadData()
        
        Btn_FindFriend.tag = 0
        Btn_Request.tag = 0
        Btn_MyFriend.tag = 1
        
        Btn_FindFriend.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_FindFriend.userInteractionEnabled=true
        Btn_Request.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        Btn_Request.userInteractionEnabled=true
        Btn_MyFriend.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        Btn_MyFriend.userInteractionEnabled=false
        
        self.FindFriendsSB.resignFirstResponder()
        self.MyFriendSB.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchBar.isEqual(FindFriendsSB)
        {
            let predicate=NSPredicate(format: "SELF.user_name CONTAINS[cd] %@", searchText)
            let arr=(AllFindName as NSArray).filteredArrayUsingPredicate(predicate)
            
            if arr.count > 0
            {
                AllFindUser.removeAll(keepCapacity: true)
                AllFindUser=arr
            }
            else
            {
                AllFindUser=AllFindName
            }
            FindFriendsTable.reloadData()
        }
        else if searchBar.isEqual(MyFriendSB)
        {
            let predicate=NSPredicate(format: "SELF.user_name CONTAINS[cd] %@", searchText)
            let arr=(AllFriendName as NSArray).filteredArrayUsingPredicate(predicate)
            
            if arr.count > 0
            {
                AllFriendUser.removeAll(keepCapacity: true)
                AllFriendUser=arr
            }
            else
            {
                AllFriendUser=AllFriendName
            }
            MyFriendTable.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar){}
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar){}
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        FindFriendsSB.resignFirstResponder()
        MyFriendSB.resignFirstResponder()
    }
    

    // All Table Methods....
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView.isEqual(FindFriendsTable)
        {
            return AllFindUser.count
        }
        else if tableView.isEqual(RequestTable)
        {
            return AllRequestUser.count
        }
        else if tableView.isEqual(MyFriendTable)
        {
            return AllFriendUser.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 48
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UserCell! = tableView.dequeueReusableCellWithIdentifier("UserCell") as? UserCell
        
        let headerView=UIView()
        headerView.backgroundColor=UIColor(patternImage: UIImage(named:"user_ra_bg_i6.png")!)
        cell.backgroundView=headerView
        cell.backgroundColor=UIColor.clearColor()

        var Data:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()

        if tableView.isEqual(FindFriendsTable)
        {
            Data = AllFindUser[indexPath.section] as! Dictionary<String, AnyObject>
            
            cell.Status.setImage(UIImage(named:"frd_invt.png")!, forState: UIControlState.Normal)
            cell.Status.addTarget(self, action: #selector(Friends.InviteAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.Status.tag=Int((Data["user_id"] as? String)!)!
            cell.Status.setTitle(Data["user_name"] as? String, forState: UIControlState.Disabled)
        }
        else if tableView.isEqual(RequestTable)
        {
            Data = AllRequestUser[indexPath.section] as! Dictionary<String, AnyObject>
            
            cell.Status.setImage(UIImage(named:"frd_dec.png")!, forState: UIControlState.Normal)
            cell.Status.addTarget(self, action: #selector(Friends.RequestDeclineAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.Status.tag=Int((Data["user_id"] as? String)!)!
            
            cell.Accept.hidden=false
            cell.Accept.setImage(UIImage(named:"frd_ace.png")!, forState: UIControlState.Normal)
            cell.Accept.addTarget(self, action: #selector(Friends.RequestAcceptAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.Accept.tag=Int((Data["user_id"] as? String)!)!
        }
        else if tableView.isEqual(MyFriendTable)
        {
            Data = AllFriendUser[indexPath.section] as! Dictionary<String, AnyObject>
            
            cell.Status.setImage(UIImage(named:"icon_chat.png")!, forState: UIControlState.Normal)
            cell.Status.addTarget(self, action: #selector(Friends.ChatAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.Status.tag=Int((Data["user_id"] as? String)!)!
            cell.Status.setTitle(Data["user_name"] as? String, forState: UIControlState.Disabled)
        }
        
        cell.imgUser.setImageWithUrl(NSURL(string:Data["user_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
        cell.imgUser.layer.cornerRadius=19
        cell.lblName.text=Data["user_name"] as? String
        
        let selectView=UIView()
        selectView.backgroundColor=UIColor.blackColor()
        cell.selectedBackgroundView=selectView
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.FindFriendsSB.resignFirstResponder()
        self.MyFriendSB.resignFirstResponder()
        self.FriendProfileView.hidden=false
        Btn_Invite_Friend.hidden=true
        
        var Data:Dictionary<String,AnyObject> = Dictionary<String, AnyObject>()

        if tableView.isEqual(FindFriendsTable)
        {
            Btn_Invite_Friend.hidden=false
            Data = AllFindUser[indexPath.section] as! Dictionary<String, AnyObject>
        }
        else if tableView.isEqual(RequestTable)
        {
            Data = AllRequestUser[indexPath.section] as! Dictionary<String, AnyObject>
        }
        else if tableView.isEqual(MyFriendTable)
        {
            Data = AllFriendUser[indexPath.section] as! Dictionary<String, AnyObject>
        }
        
        seleted_id=Int((Data["user_id"] as? String)!)!
        FriendPic.setImageWithUrl(NSURL(string:Data["user_image"]! as! String)!, placeHolderImage: UIImage(named:"team_bg"))
        FriendPic.layer.cornerRadius=27
        FriendPic.layer.masksToBounds = true

        Name.text=Data["user_name"] as? String
        Email.text=Data["email"] as? String
        NumBet.text="\(Data["totalbets"] as! String) Times Bet"
        
        self.FriendProfileView.frame = self.setFrameX(self.FriendProfileView.frame, x: self.FriendProfileView.frame.size.width)
        UIView.animateWithDuration(0.4, animations:
            {
                self.FriendProfileView.frame = self.setFrameX(self.FriendProfileView.frame, x: 0)
            },
            completion:
            {(value: Bool) in
        })
    }
    
    func InviteAction(sender: UIButton!)
    {
        Btn_Invite_Friend.tag = 1
        seleted_id=sender.tag
        seleted_name=sender.titleForState(UIControlState.Disabled)!
        
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopView.hidden=false
                self.AlrtMessage.text="Are you sure send friend invitation to \(self.seleted_name) ?"
            },
            completion:
            {(value: Bool) in
        })
    }
    
    @IBAction func Invitation_Btn_Clicked(sender: AnyObject)
    {
        Btn_Invite_Friend.tag = 2
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopView.hidden=false
                self.AlrtMessage.text="Are you sure send friend invitation to \(self.Name.text!) ?"
            },
            completion:
            {(value: Bool) in
        })
    }
    
    @IBAction func Alrt_Invite_Clicked(sender: AnyObject)
    {
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        var dataDict=ApiObj.CallPostApi("user/friendrequest/\(urlStr)", postStr: "user_id=\(seleted_id)")
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                self.FindFriends_Btn_Clicked(Btn_FindFriend)
                if Btn_Invite_Friend.tag == 2
                {
                    self.FriendProfileView.frame = self.setFrameX(self.FriendProfileView.frame, x: self.FriendProfileView.frame.size.width)
                    self.FriendProfileView.hidden=true
                }
                
                UIView.animateWithDuration(0.4, animations:
                    {
                        self.PopView.hidden=true
                    },
                    completion:
                    {(value: Bool) in
                })
            }
        }
    }
    
    @IBAction func Alrt_Decline_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.PopView.hidden=true
            },
            completion:
            {(value: Bool) in
        })
    }

    @IBAction func Back_Profile_Clicked(sender: AnyObject)
    {
        UIView.animateWithDuration(0.4, animations:
            {
                self.FriendProfileView.frame = self.setFrameX(self.FriendProfileView.frame, x: self.FriendProfileView.frame.size.width)
            },
            completion:
            {(value: Bool) in
                self.FriendProfileView.hidden=true
        })
    }
    
    func RequestAcceptAction(sender: UIButton!)
    {
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "user_id=\(sender.tag)&action=accept"
        var dataDict=ApiObj.CallPostApi("user/requestacceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                self.Request_Btn_Clicked(Btn_Request)
            }
        }
    }
    
    func RequestDeclineAction(sender: UIButton!)
    {
        let urlStr=NSUserDefaults.standardUserDefaults().objectForKey("token") as! String
        let postString = "user_id=\(sender.tag)&action=declined"
        var dataDict=ApiObj.CallPostApi("user/requestacceptdecline/\(urlStr)", postStr: postString)
        
        if (dataDict.null == nil)
        {
            if dataDict == 6
            {
                TWMessageBarManager.sharedInstance().showMessageWithTitle("Network error", description: "Couldn't connect to the server. Check your network connection.", type: TWMessageBarMessageType.Error, statusBarHidden: false, callback: { () -> Void in})
            }
            else if let _ = dataDict["msg"].string
            {
                self.Request_Btn_Clicked(Btn_Request)
            }
        }
    }
    
    func ChatAction(sender: UIButton!)
    {
        seleted_id=sender.tag
        seleted_name=sender.titleForState(UIControlState.Disabled)!

        self.performSegueWithIdentifier("GoToChatting", sender: self)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "GoToChatting"
        {
            let sc = (segue.destinationViewController as! ChattingView)
            sc.user_id=self.seleted_id
            sc.user_name=self.seleted_name
        }
    }

}
